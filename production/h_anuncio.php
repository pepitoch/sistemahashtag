<?php
	session_start(); 
	//session_destroy();

	//Data
	include_once "data/dataBase.php";
	
	//Clases
	include_once "objects/clases/cAnuncio.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oAnuncio = new Anuncio($db);
	
	$vAnuncioPremium	= $oAnuncio->get_count_anunciop();
	$vAnuncioBasic		= $oAnuncio->get_count_anunciob();
	$vAnuncioAll		= $oAnuncio->get_count_anuncio();
	
	
	// set your default timezone
	date_default_timezone_set('America/Tegucigalpa');
	setlocale(LC_ALL,"es_SV");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

	
	//echo $_SESSION['imgFile'].'d';
	//echo phpinfo();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Sistema HT #</title>
	
	
	<?php include_once "c_css.php";?>

    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Publicar Anuncio <small class="green">Bolsa de Trabajo</small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			
			
            <div class="clearfix"></div>
			
			<form class="form-horizontal form-label-left" method="POST" novalidate enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-8 col-sm-6 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Ejecutivo de Ventas</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="code-ventas">C&oacute;digo Ejecutivo de Ventas <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="code-ventas" name="code-ventas" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Completo <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="first-name" name="first-name" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							  <div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Correo Electr&oacute;nico <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback-left">
								  <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
								  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
						  </div>
					  </div>
					</div>
					
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Anuncios Solicitados </h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<h4>Bolsa de Trabajo <small>para la semana <?= 'Lunes '. date('d', strtotime('monday next week'))  .' '. $meses[date('n', strtotime('monday next week'))-1]?> - <?= 'Domingo '. date('d', strtotime('sunday next week'))  .' '. $meses[date('n', strtotime('sunday next week'))-1]?></small></h4>
							<hr/>
							<h4>Tipo de Anuncio</h4>
							<div class="widget_summary">
							  <div class="w_left w_25">
								<span>Empresa Destacada</span>
							  </div>
							  <div class="w_center w_55">
								<div class="progress">
								  <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?= ($vAnuncioAll==0) ? 0 : ($vAnuncioPremium / $vAnuncioAll) * 100;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=  ($vAnuncioAll==0) ? 0 : ($vAnuncioPremium / $vAnuncioAll) * 100;?>%;">
									<span class=""><?= ($vAnuncioAll==0) ? 0 : number_format(($vAnuncioPremium / $vAnuncioAll) * 100, 2, '.', '');?>% anuncios desatacados</span>
								  </div>
								</div>
							  </div>
							  <div class="w_right w_20">
								<span><?=$vAnuncioPremium;?></span>
							  </div>
							  <div class="clearfix"></div>
							</div>

							<div class="widget_summary">
							  <div class="w_left w_25">
								<span>Empresa solo Explorador</span>
							  </div>
							  <div class="w_center w_55">
								<div class="progress">
								  <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?= ($vAnuncioAll==0) ? 0 : ($vAnuncioBasic / $vAnuncioAll) * 100;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($vAnuncioAll==0) ? 0 : ($vAnuncioBasic / $vAnuncioAll) * 100;?>%;">
									<span class=""><?= ($vAnuncioAll==0) ? 0 : number_format(($vAnuncioBasic / $vAnuncioAll) * 100, 2, '.', '');?>% anuncios explorador</span>
								  </div>
								</div>
							  </div>
							  <div class="w_right w_20">
								<span><?=$vAnuncioBasic;?></span>
							  </div>
							  <div class="clearfix"></div>
							</div>
							
						  </div>
						</div>
					  </div>
				</div>
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Cliente</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							
								<div class="item form-group">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<label>Tipo de anuncio a publicar *:</label>
										<div class="sidebar-widget">
											<div class="radio">
											  <label>
												<input onclick="show1();" type="radio" name="tipoanuncio" id="tipoanuncio1" value="1" checked required="required">
												
												Anuncio b&aacute;sico para <b>Explorador de Empleos</b>
												
											  </label>
											</div>
										
											<div class="radio">
											  <label>
												<input onclick="show2();" type="radio" name="tipoanuncio" id="tipoanuncio2" value="2" required="required">
												Anuncio premium para <b>Empresas Destacadas</b>
											  </label>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
																				
										<div class="col-md-6">
											<div class="thumbnail">
											  <div class="image view view-first">
												<img style="width: 100%; display: block;" src="images/anuncio_basico.jpg" alt="image" />
												<div class="mask">
												  
												  <div class="tools tools-bottom">
													<a href="#" data-toggle="modal" data-target=".bs-basic-modal-lg"><i class="fa fa-search-plus"></i> Ampliar Imagen</a>
													
												  </div>
												</div>
											  </div>
											  <div class="caption">
												<p>Ejemplo Anuncio Basico</p>
											  </div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="thumbnail">
											  <div class="image view view-first">
												<img style="width: 100%; display: block;" src="images/anuncio_premium.jpg" alt="image" />
												<div class="mask">
												
												  <div class="tools tools-bottom">
													<a href="#" data-toggle="modal" data-target=".bs-premium-modal-lg"><i class="fa fa-search-plus"></i> Ampliar Imagen</a>
													
												  </div>
												</div>
											  </div>
											  <div class="caption">
												<p>Ejemplo Anuncio Premium</p>
											  </div>
											</div>
										</div>
										
										<div class="modal fade bs-basic-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">

												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
												  </button>
												  <h4 class="modal-title" id="myModalLabel">Anuncio Basico</h4>
												</div>
												<div class="modal-body">
													<img class="img-responsive" src="images/anuncio_basico.jpg" alt="image" />
												</div>
												<div class="modal-footer">
												  <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
												</div>

											  </div>
											</div>
										</div>
										
										<div class="modal fade bs-premium-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">

												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
												  </button>
												  <h4 class="modal-title" id="myModalLabel">Anuncio Premium</h4>
												</div>
												<div class="modal-body">
													<img class="img-responsive" src="images/anuncio_premium.jpg" alt="image" />
												</div>
												<div class="modal-footer">
												  <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
												</div>

											  </div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="divider-dashed"></div>	
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
									
										<label>Nombre de la empresa: *</label>
										<input type="text" class="form-control" name="nEmpresa" placeholder="Nombre de empresa a publicar" required="required">
										<p class="help-block">Si alguna empresa quiere el anonimato colocar un nombre que pueda identificar el anuncio por ejemplo: nombre del ejecutivo de ventas o de alguna persona responsable del anuncio.</p>
									</div>
								</div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="form-group">
										<label>Descripcion de la empresa:</label>
										<input type="text" class="form-control" name="nDescripcion" placeholder="Descripcion de empresa a publicar">
										<p class="help-block">Puede ser el nombre completo de la empresa o el mismo texto de nombre. Este campo no es obligatorio.</p>
									</div>
								</div>
								
								<div class="clearfix"></div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
									
										<label>Correo electronico de la empresa: *</label>
										<input type="email" class="form-control" name="nEmail" placeholder="Correo electronico para Hoja de Vida" required="required">
										<p class="help-block">Correo electronico donde se enviaran las Hojas de Vida.</p>
									</div>
								</div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
										<label>Imagen / PDF del anuncio: </label>
										<input type="file" class="form-control" name="filelife" id="imagenanuncio" >
										<p class="help-block">Imagen o PDF del anuncio a publicar.</p>
									</div>
								</div>
								
						  </div>
						  
							<div id="anuncio-premium">
								<div class="x_title">
									<br/>
									<h2><br/><br/>Informaci&oacute;n de Especial para Empresas Destacadas</h2>
									
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
							  
									
									
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<label>Frase o Historia de la empresa: </label>
											<textarea name="nFrase" class="form-control" rows="4"></textarea>
										</div>
										
									</div>
									
									<div class="divider-dashed"></div>	
									<p>&nbsp;</p>
									
									<div class="form-group text-center">
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-internet-explorer fa-3x"></span></label>
											<input type="text" class="form-control" name="nWeb" placeholder="URL de sitio web">
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-facebook fa-3x"></span></label>
											<input type="text" class="form-control" name="nFacebook" placeholder="URL de facebook">
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-twitter fa-3x"></span></label>
											<input type="text" class="form-control" name="nTwitter" placeholder="URL de twitter">
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-youtube-play fa-3x"></span></label>
											<input type="text" class="form-control" name="nYoutube" placeholder="URL de youtube">
										</div>
										
									</div>
									
									
									
									
								</div>
							</div>
							
							<div class="clearfix"></div>
							<br/>
							<div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" value="nAnuncio" name="opt" />
									<button id="send" type="submit" class="btn btn-success btn-block btn-lg">Enviar Publicacion de Bolsa de Trabajo</button>
								</div>

							</div>
					  </div>
					</div>
				</div>
			</form>
			
			
           

            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div>
	
	<!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    

    
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
   
    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert.min.js"></script>
	
	<!-- jquery.cookie.js -->
	<script src="../vendors/js-cookie-master/js-cookie-master/src/js.cookie.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

	
	<script type="text/javascript">
		
		function show1(){
		  document.getElementById('anuncio-premium').style.display ='none';
		}
		function show2(){
		  document.getElementById('anuncio-premium').style.display = 'block';
		}

	</script>
	
    
   
	<!-- Jquery cookie -->
	<script>
		if(Cookies.get('codigo')){
			$('#code-ventas').val(Cookies.get('codigo'));
			$('#first-name').val(Cookies.get('nombre'));
			$('#email').val(Cookies.get('correo'));
			$('.myname').text(Cookies.get('nombre'));
		}
	</script>
	
	
    <!-- validator -->
    <script>
      // initialize the validator function
     // validator.message.inversion = 'not a real date';

		var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})
		

		$('form').submit(function(e){
			
			
			
			var submit = true, validatorResult = validator.checkAll(this);
			
			if (!validatorResult.valid) {
				console.log('holis');
				submit = false;
				
			}
			
			if (submit){
				$('#load-form').fadeIn();
				console.log('submit');
				
				//$(document).on('submit', '#create-business-form', function() {
					$.ajax({
					  url: "objects/actionAnuncio.php",
					  //url: "http://127.0.0.1/SistemaHashtag/production/objects/actionAnuncio.php",
					  type: "POST",
					  data:  new FormData(this),
					  contentType: false,
					  cache: false,
					  processData:false,
					  beforeSend : function(){
						
					  },
					  success: function(data) {
							Cookies.set('codigo', $('#code-ventas').val(), { expires: 365 });
							Cookies.set('nombre', $('#first-name').val(), { expires: 365 });
							Cookies.set('correo', $('#email').val(), { expires: 365 });
							
						var parsed = JSON.parse(data);
						  swal({
							title: parsed.title,
							text: parsed.text,
							icon: parsed.type,
							button: "Aww yiss!"
						  });
							$('form')[0].reset();
							$('#load-form').fadeOut();
							
							if(Cookies.get('codigo')){
								$('#code-ventas').val(Cookies.get('codigo'));
								$('#first-name').val(Cookies.get('nombre'))
								$('#email').val(Cookies.get('correo'))
							}
						},
						error: function(e) { 
							swal({   
								title: 'Warning',
								text: 'Revisa la informacion proporcionada.',
								type: 'warning',
								confirmButtonText: "Ok"
							});
							$('form')[0].reset();
							$('#load-form').fadeOut();
						}
					});
					return false;
					
				//});
			}
			
			return !!validatorResult.valid;
		});
      

      
    </script>
    <!-- /validator -->

	
	
  </body>
</html></html>