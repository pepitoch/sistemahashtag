<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

header("access-control-allow-origin: *");

// DataBase
include_once '../data/dataBase.php';
 

// Classes
include_once 'clases/cAnuncio.php';

include_once "clases/cConsumo.php";

define ('PATH', URL_BT.'API/');

$objctConsumo = new Consumo();

$database = new Database();
$db = $database->getConnection();

$oAnuncio 	= new Anuncio($db);

$option = '';
$upload_dir = '';
$productpic = '';
$errorMsg = '';


if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

function generateRandomString($length = 3) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function sendMailAnuncio($params) {
	//session_destroy();
	
	
	//Email information
	$admin_email = CORREOS . $params[3];
	
	$email = $params[3];
	$subject = 'Publicar banner para Bolsa de Trabajo para ' . $params[2];
	$comment = 'El ejecutivo de ventas ' . $params[2] . ' quiere publicar un Banner de bolsa de Trabajo con el cliente ' . $params[4] . "\xA" .
				'No. Ticket: ' . $params[0] . "\xA" .
				'Codigo de Vendedor: ' . $params[1] . "\xA" .
				'Nombre: ' . $params[2] . "\xA" .
				'Email: ' . $params[3] . "\xA" .
				"------------------------------------------------- \xA" .
				'Cliente: ' . $params[4] . "\xA" .
				'Correo de Cliente: ' . $params[7] . "\xA" .
				'Tipo de Anuncio: ' . (($params[12] == 1)? 'Explorador de Empleos' : 'Empresa Destacada') . "\xA" .
				'Frase: ' . $params[14] . "\xA" .
				
				"------------------------------------------------- \xA" .
				'Solicitud enviada: ' . date('Y-m-d') . "\xA" 
				
				;
	  
	// //send email
	//echo $admin_email . $subject . $comment . $email;
	if(DEV != true){
		mail($admin_email, "$subject", $comment, "From:" . $email);
	}
	
	
}


//Guarda Nuevo Usuario Web
if ( $option == 'nAnuncio' ) {
	try{
		//parametros 
		$trackid				= generateRandomString().'-'.generateRandomString().'-'.generateRandomString();
		$codeventas				= $_POST['code-ventas'];
		$nombre					= $_POST['first-name'];
		$email					= $_POST['email'];
		
		$tipoanuncio			= $_POST['tipoanuncio'];//1 basico - 2 premium
		
		$nEmpresa				= $_POST['nEmpresa'];
		$nDescripcion			= $_POST['nDescripcion'];
		$nEmail					= $_POST['nEmail'];
		
		if($_FILES['filelife']['name']!=NULL){
			$imgFile = $_FILES['filelife']['name'];
			$tmp_dir = $_FILES['filelife']['tmp_name'];
			$imgSize = $_FILES['filelife']['size'];
			$upload_dir = '../images/anunciosBT/'; // upload directory						
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
					// valid image extensions

			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'pdf', 'docx'); // valid extensions
			// rename uploading image
			$productpic = "bt_".$nEmail . "_" . time() . "." . $imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// Check file size '5MB'
				if($imgSize < 5000000){
					
					move_uploaded_file($tmp_dir,$upload_dir.$productpic);
					
					$errorMsg = 'Anuncio subido, el proximo Lunes estara disponible en la platafroma Bolsa de Trabajo';
					
				}	else {
					//echo "Lo sentimos, archivo muy pesado.";
					$errorMsg = 'Archivo excede los 5Megas';
				}
			}	else {
				
				//echo "Lo sentimos, solo JPG, JPEG, PNG & GIF tipo de archivos soportados.";
				$errorMsg = 'Los archivos permitidos son imagenes, PDF y documentos tipo docx';
			}
		}
		
		
		$fIngreso					= date("Y-m-d H:i:s");
		$fInicio					= date("Y-m-d H:i:s");
		$fFin 						= date("Y-m-d H:i:s");
		
		$estado						= '0';
		$idFormulario				= '2';	


		//premium
		$nFrase					= $_POST['nFrase'];
		$nWeb					= $_POST['nWeb'];
		$nFacebook				= $_POST['nFacebook'];
		$nTwitter				= $_POST['nTwitter'];
		$nYoutube				= $_POST['nYoutube'];
		
		
			
		$params = array($trackid,$codeventas,$nombre,$email,$nEmpresa,$nDescripcion,$upload_dir.$productpic,$nEmail,$fIngreso,$fInicio,$fFin,$estado,$tipoanuncio,$idFormulario,$nFrase,$nWeb,$nFacebook,$nTwitter,$nYoutube);
		
		$save   = $oAnuncio->nuevo($params);
	
		if ( $save ) {
			
			sendMailAnuncio($params);
			echo json_encode(array("title" => "Anuncio ingresado exitosamente", "text" => "ok $errorMsg", "type" => "success"));
									
		} else {
			echo json_encode(array("title" => "Advertencia", "text" => "Revisa la informacion proporcionada $errorMsg.", "type" => "warning"));
		}
		
		
		
		
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. $errorMsg", "type" => "error"));
	}
}



//Guarda Nuevo Usuario Web en Web Service 
if ( $option == 'wsAnuncio' ) {
	try{
		
		$estado_anuncio			 	= $_POST['status'];
		$tipo_anuncio			 	= $_POST['tipoanuncio'];
		$nombre_empresa 			= $_POST['nEmpresa'];
		$descripcion_empresa 		= $_POST['nDescripcion'];
		$email_empresa 				= $_POST['nEmail'];
		
		//imagen basica o logo premium
		$file_name 		= $_FILES['filelife']['name'];
		$file_ext	 	= strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
		$file_size		= $_FILES['filelife']['size'];
		$file_tmp		= $_FILES['filelife']['tmp_name'];
		$type 			= pathinfo($file_tmp, PATHINFO_EXTENSION);
		$data 			= file_get_contents($file_tmp);
		$base64 		= 'data:image/' . $file_ext . ';base64,' . base64_encode($data);
		
		//echo "Base64 is ".$base64;
		//echo $_POST['tipoanuncio'];
		
		if ($tipo_anuncio == '1'){
			//guardar anunio basico
			$arrRedSocial = array( 
				"idred_social"	=> '',
				"link"			=> ''
			);
			$arrPlazas = array( 
				"nombre"	=> ''
			);
			$body_transac = array( 
				"nombre"				=> $nombre_empresa,
				"descripcion"			=> $descripcion_empresa ,
				"imagen"				=> $base64,
				"repuesta_correo"		=> $email_empresa,
				"fecha_ingreso"			=> date('Y-m-d h:m:s'),
				"fecha_inicio"			=> date('Y-m-d h:m:s'),
				"fecha_fin"				=> date('Y-m-d h:m:s'),
				"estado"				=> $estado_anuncio,
				"idtipo_anuncio"		=> $tipo_anuncio,
				"idformulario"			=> '2',
				"fondo"					=> '',
				"frase"					=> '',
				"logo"					=> '',
				"website"				=> '',
				"telefono"				=> '',
				"correo"				=> '',
				"direccion"				=> '',
				"fondo2"				=> '',
				"red_social"			=> '',
				"plazas"				=> ''	
			);
			$body 		= json_encode($body_transac);
			$url 		= PATH."setRegistroAnuncio";
			$result_c	= $objctConsumo->postConsumo($url,$body);
			$objt_c 	= json_decode($result_c);
			
			if($objt_c){
				//echo 'guardar anuncio basico ' . $objt_c;
				if($objt_c->errorCode == 0){
					
					$mensaje_titulo = "Anuncio publicado con exito";
					$mensaje_cuerpo = "Ver anuncio en linea";
					
					//update a la tabla de tickets a 1
					if(isset($_POST['status_ticket'])){
						$oAnuncio->update_estado_anuncio($_POST['status_ticket']);
					}
					
					
				} else{
					$mensaje_titulo = "Error no identificado1";
					$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
				}
			}else{
				//echo 'eERROR anuncio basico ' . $objt_c;
				$mensaje_titulo = "Error no identificado2";
				$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
			}
			
			echo json_encode(array("title" => "Success", "text" => "ok $mensaje_titulo $mensaje_cuerpo", "type" => "success"));
			
			
		} else if ($tipo_anuncio == '2'){
			
			//guardar anuncio premium
			
		
			//imagen basica o logo premium FONDO
			$file_nameP1 		= $_FILES['imgFondo']['name'];
			$file_extP1	 		= strtolower(pathinfo($file_nameP1,PATHINFO_EXTENSION));
			$file_sizeP1		= $_FILES['imgFondo']['size'];
			$file_tmpP1			= $_FILES['imgFondo']['tmp_name'];
			$typeP1 			= pathinfo($file_tmpP1, PATHINFO_EXTENSION);
			$dataP1 			= file_get_contents($file_tmpP1);
			$base64P1 			= 'data:image/' . $file_extP1 . ';base64,' . base64_encode($dataP1);
			
			//imagen basica o logo premium FONDO Middle
			$file_nameP2 		= $_FILES['imgFondo2']['name'];
			$file_extP2	 		= strtolower(pathinfo($file_nameP2,PATHINFO_EXTENSION));
			$file_sizeP2		= $_FILES['imgFondo2']['size'];
			$file_tmpP2			= $_FILES['imgFondo2']['tmp_name'];
			$typeP2 			= pathinfo($file_tmpP1, PATHINFO_EXTENSION);
			$dataP2 			= file_get_contents($file_tmpP2);
			$base64P2 			= 'data:image/' . $file_extP2 . ';base64,' . base64_encode($dataP2);
			
			//imagen basica o logo premium LOGO MARCA
			$file_nameP3 		= $_FILES['imgLogoM']['name'];
			$file_extP3	 		= strtolower(pathinfo($file_nameP3,PATHINFO_EXTENSION));
			$file_sizeP3		= $_FILES['imgLogoM']['size'];
			$file_tmpP3			= $_FILES['imgLogoM']['tmp_name'];
			$typeP3 			= pathinfo($file_tmpP3, PATHINFO_EXTENSION);
			$dataP3 			= file_get_contents($file_tmpP3);
			$base64P3 			= 'data:image/' . $file_extP3 . ';base64,' . base64_encode($dataP3);
			
			$frase 				= $_POST['nFrase'];
			$website			= $_POST['nWeb'];
			$phone 				= $_POST['nPhone'];
			$direccion 				= $_POST['nDireccion'];
			
			//REdes sociales
			$inputRedSocial		= $_POST['nRedSocial'];
			$arrInputRedSocial  = array();
			
			foreach( $inputRedSocial as $key => $n ) {
				if($n != ''){
					if($key == 0){
						$arrInputRedSocial[] = array(
							"idred_social" => '100',
							"link" => $n
						);
					} else if($key == 1){
						$arrInputRedSocial[] = array(
							"idred_social" => '101',
							"link" => $n
						);
					} else if($key == 2){
						$arrInputRedSocial[] = array(
							"idred_social" => '102',
							"link" => $n
						);
					} else if($key == 3){
						$arrInputRedSocial[] = array(
							"idred_social" => '103',
							"link" => $n
						);
					}
				}
				
				
			}
			
			$arrRedSocial = $arrInputRedSocial;
			
			
			//Plazas disponibvles
			$inputPlazas		= $_POST['plaza'];
			$arrInputPlaza		= array();
			
			foreach( $inputPlazas as $key => $n ) {
				if($n != ''){
					$arrInputPlaza[] = array(
						"nombre" => $n
					);
				}
				
			}
			
			$arrPlazas = $arrInputPlaza;
			
			$body_transac = array( 
				"nombre"				=> $nombre_empresa,
				"descripcion"			=> $descripcion_empresa ,
				"imagen"				=> $base64,
				"repuesta_correo"		=> $email_empresa,
				"fecha_ingreso"			=> date('Y-m-d h:m:s'),
				"fecha_inicio"			=> date('Y-m-d h:m:s'),
				"fecha_fin"				=> date('Y-m-d h:m:s'),
				"estado"				=> $estado_anuncio,
				"idtipo_anuncio"		=> $tipo_anuncio,
				"idformulario"			=> '2',
				"fondo"					=> $base64P1,
				"frase"					=> $frase,
				"logo"					=> $base64P3,
				"website"				=> $website,
				"telefono"				=> $phone,
				"correo"				=> $email_empresa,
				"direccion"				=> $direccion,
				"fondo2"				=> $base64P2,
				"red_social"			=> $arrRedSocial,
				"plazas"				=> $arrPlazas	
			);
			
			$body 		= json_encode($body_transac);
			//echo $body;
			$url 		= PATH."setRegistroAnuncio";
			$result_c	= $objctConsumo->postConsumo($url,$body);
			$objt_c 	= json_decode($result_c);
			//echo $result_c;
			if($objt_c){
				//echo 'guardar anuncio basico ' . $objt_c;
				if($objt_c->errorCode == 0){
					
					$mensaje_titulo = "Anuncio publicado con exito";
					$mensaje_cuerpo = "Ver anuncio en linea";
					
					
					//update a la tabla de tickets a 1
					if(isset($_POST['status_ticket'])){
						$oAnuncio->update_estado_anuncio($_POST['status_ticket']);
					}
					
				} else{
					$mensaje_titulo = "Error no identificado1";
					$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
				}
			}else{
				//echo 'eERROR anuncio basico ' . $objt_c;
				$mensaje_titulo = "Error no identificado2";
				$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
			}
			
			echo json_encode(array("title" => "Success", "text" => "ok $mensaje_titulo $mensaje_cuerpo", "type" => "success"));
			
		}
		
			
	}catch (Exception $e){
		//var_dump ( $e );
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. $errorMsg", "type" => "error"));
	}
	
	
	
	
}




//Guarda Nuevo Usuario Web en Web Service 
if ( $option == 'wsAnuncioUpdate' ) {
	try{
		
		$idanuncio_c			 	= $_POST['idanuncio_c'];
		$estado_anuncio			 	= $_POST['status'];
		$tipo_anuncio			 	= $_POST['tipoanuncio'];
		$nombre_empresa 			= $_POST['nEmpresa'];
		$descripcion_empresa 		= $_POST['nDescripcion'];
		$email_empresa 				= $_POST['nEmail'];
		
		if($tipo_anuncio == 1){
			
			if ($_FILES['filelife']['size'] == 0){
				// cover_image is empty (and not an error) no hay imagen
				
				$body_transac = array( 
					"idanuncio"				=> $idanuncio_c,
					"nombre"				=> $nombre_empresa,
					"descripcion"			=> $descripcion_empresa ,
					"imagen"				=> '',
					"repuesta_correo"		=> $email_empresa,
					"idtipo_anuncio"		=> $tipo_anuncio,
					"fecha_inicio"			=> date('Y-m-d h:m:s'),
					"fecha_fin"				=> date('Y-m-d h:m:s'),
					"estado"				=> $estado_anuncio
					
				);
				$body 		= json_encode($body_transac);
				$url 		= PATH."updateRegistroAnuncioBasico";
				$result_c	= $objctConsumo->putConsumo($url,$body);
				$objt_c 	= json_decode($result_c);
				
				//var_dump($result_c);
				
				if($objt_c){
					//echo 'guardar anuncio basico ' . $objt_c;
					if($objt_c->errorCode == 0){
						
						$mensaje_titulo = "Anuncio modificado con exito noimg";
						$mensaje_cuerpo = "Ver anuncio en linea";
						
						
					} else{
						$mensaje_titulo = "Error no identificado1";
						$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
					}
				}else{
					//echo 'eERROR anuncio basico ' . $objt_c;
					$mensaje_titulo = "Error no identificado2";
					$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
				}
				
				echo json_encode(array("title" => "Success", "text" => "ok $mensaje_titulo $mensaje_cuerpo", "type" => "success"));
				
				
			} else {
				
				//si hay imagen
				
				//imagen basica o logo premium
				$file_name 		= $_FILES['filelife']['name'];
				$file_ext	 	= strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
				$file_size		= $_FILES['filelife']['size'];
				$file_tmp		= $_FILES['filelife']['tmp_name'];
				$type 			= pathinfo($file_tmp, PATHINFO_EXTENSION);
				$data 			= file_get_contents($file_tmp);
				$base64 		= 'data:image/' . $file_ext . ';base64,' . base64_encode($data);
				
				$body_transac = array( 
					"idanuncio"				=> $idanuncio_c,
					"nombre"				=> $nombre_empresa,
					"descripcion"			=> $descripcion_empresa ,
					"imagen"				=> $base64,
					"repuesta_correo"		=> $email_empresa,
					"idtipo_anuncio"		=> $tipo_anuncio,
					"fecha_inicio"			=> date('Y-m-d h:m:s'),
					"fecha_fin"				=> date('Y-m-d h:m:s'),
					"estado"				=> $estado_anuncio
					
				);
				$body 		= json_encode($body_transac);
				$url 		= PATH."updateRegistroAnuncioBasico";
				$result_c	= $objctConsumo->putConsumo($url,$body);
				$objt_c 	= json_decode($result_c);
				
				if($objt_c){
					//echo 'guardar anuncio basico ' . $objt_c;
					if($objt_c->errorCode == 0){
						
						$mensaje_titulo = "Anuncio modificado con exito img";
						$mensaje_cuerpo = "Ver anuncio en linea";
						
						
					} else{
						$mensaje_titulo = "Error no identificado1 X";
						$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
					}
				}else{
					//echo 'eERROR anuncio basico ' . $objt_c;
					$mensaje_titulo = "Error no identificado2 X";
					$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
				}
				
				echo json_encode(array("title" => "Success", "text" => "ok $mensaje_titulo $mensaje_cuerpo", "type" => "success"));
				
			}
		
		
		} else if($tipo_anuncio == 2){
			
			if ($_FILES['filelife']['size'] != 0){
				//imagen basica o logo premium
				$file_name 		= $_FILES['filelife']['name'];
				$file_ext	 	= strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
				$file_size		= $_FILES['filelife']['size'];
				$file_tmp		= $_FILES['filelife']['tmp_name'];
				$type 			= pathinfo($file_tmp, PATHINFO_EXTENSION);
				$data 			= file_get_contents($file_tmp);
				$base64 		= 'data:image/' . $file_ext . ';base64,' . base64_encode($data);
				
			}
			
			if ($_FILES['imgFondo']['size'] != 0){
				//imagen basica o logo premium
				$file_nameP1 		= $_FILES['imgFondo']['name'];
				$file_extP1	 		= strtolower(pathinfo($file_nameP1,PATHINFO_EXTENSION));
				$file_sizeP1		= $_FILES['imgFondo']['size'];
				$file_tmpP1			= $_FILES['imgFondo']['tmp_name'];
				$typeP1 			= pathinfo($file_tmpP1, PATHINFO_EXTENSION);
				$dataP1 			= file_get_contents($file_tmpP1);
				$base64P1 			= 'data:image/' . $file_extP1 . ';base64,' . base64_encode($dataP1);
				
			}
			
			if ($_FILES['imgFondo2']['size'] != 0){
				//imagen basica o logo premium
				$file_nameP2 		= $_FILES['imgFondo2']['name'];
				$file_extP2	 		= strtolower(pathinfo($file_nameP2,PATHINFO_EXTENSION));
				$file_sizeP2		= $_FILES['imgFondo2']['size'];
				$file_tmpP2			= $_FILES['imgFondo2']['tmp_name'];
				$typeP2 			= pathinfo($file_tmpP2, PATHINFO_EXTENSION);
				$dataP2 			= file_get_contents($file_tmpP2);
				$base64P2 			= 'data:image/' . $file_extP2 . ';base64,' . base64_encode($dataP2);
				
			}
			
			if ($_FILES['imgLogoM']['size'] != 0){
				//imagen basica o logo premium
				$file_nameP3 		= $_FILES['imgLogoM']['name'];
				$file_extP3	 		= strtolower(pathinfo($file_nameP3,PATHINFO_EXTENSION));
				$file_sizeP3		= $_FILES['imgLogoM']['size'];
				$file_tmpP3			= $_FILES['imgLogoM']['tmp_name'];
				$typeP3 			= pathinfo($file_tmpP3, PATHINFO_EXTENSION);
				$dataP3 			= file_get_contents($file_tmpP3);
				$base64P3 			= 'data:image/' . $file_extP3 . ';base64,' . base64_encode($dataP3);
				
			}
			
			$iddetalle_anuncio			 	= $_POST['idanuncio_d'];
			$frase 							= $_POST['nFrase'];
			$website						= $_POST['nWeb'];
			$phone 							= $_POST['nPhone'];
			$direccion 						= $_POST['nDireccion'];
			
			//REdes sociales
			$inputRedSocial		= $_POST['nRedSocial'];
			$iDRedSocial		= $_POST['nRedSocialID'];
			$arrInputRedSocial  = array();
			
			foreach( $inputRedSocial as $key => $n ){
				
				$arrInputRedSocial[] = array(
					"idcatalogo_detalle_red" => $iDRedSocial[$key],
					"link" => $n
				);
			}
			
			//plazas
			$inputPlazas		= $_POST['plaza'];
			$iDPlaza			= $_POST['plazaID'];
			$arrInputPlaza		= array();
			
			foreach( $inputPlazas as $key => $n ){
				
				$arrInputPlaza[] = array(
					"idplaza" => $iDPlaza[$key],
					"nombre" => $n
				);
			}
			
			$body_transac = array( 
				"idanuncio"				=> $idanuncio_c,
				"nombre"				=> $nombre_empresa,
				"descripcion"			=> $descripcion_empresa ,
				"imagen"				=> ($_FILES['filelife']['size'] != 0)?$base64:'',
				"repuesta_correo"		=> $email_empresa,
				"fecha_inicio"			=> date('Y-m-d h:m:s'),
				"fecha_fin"				=> date('Y-m-d h:m:s'),
				"estado"				=> $estado_anuncio,
				"idtipo_anuncio"		=> $tipo_anuncio,
				"iddetalle_anuncio"		=> $iddetalle_anuncio,
				"fondo"					=> ($_FILES['imgFondo']['size'] != 0)?$base64P1:'',
				"frase"					=> $frase,
				"logo"					=> ($_FILES['imgLogoM']['size'] != 0)?$base64P3:'',
				"website"				=> $website,
				"telefono"				=> $phone,
				"correo"				=> $email_empresa,
				"direccion"				=> $direccion,
				"fondo2"				=> ($_FILES['imgFondo2']['size'] != 0)?$base64P2:'',
				"red_social"			=> $arrInputRedSocial,
				"plazas"				=> $arrInputPlaza	
			);
			$body 		= json_encode($body_transac);
			
			$url 		= PATH."updateRegistroAnuncioCustom";
			$result_c	= $objctConsumo->putConsumo($url,$body);
			$objt_c 	= json_decode($result_c);
			
			if($objt_c){
				
				if($objt_c->errorCode == 0){
					
					$mensaje_titulo = "Anuncio modificado con exito";
					$mensaje_cuerpo = "Ver anuncio en linea";
					
					
				} else{
					$mensaje_titulo = "Error no identificado1 X";
					$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
				}
			}else{
				
				$mensaje_titulo = "Error no identificado2 X";
				$mensaje_cuerpo = "Comuniquese con soporte tecnico para poder recibir ayuda con el proceso";
			}
			
			echo json_encode(array("title" => "Success", "text" => "ok $mensaje_titulo $mensaje_cuerpo", "type" => "success"));
			
			
			
		}
		
		
		
			
	}catch (Exception $e){
		//var_dump ( $e );
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. $errorMsg", "type" => "error"));
	}
	
	
	
	
}


?>