<?php
header("access-control-allow-origin: *");

// DataBase
include_once '../data/dataBase.php';


// Classes
include_once 'clases/cUser.php';

$database = new Database();
$db = $database->getConnection();

$oUser 	= new User($db);

$option = '';

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

if ($option == "nLogin") {

	$email 		= $_POST['email'];
	$password 	= $_POST['password'];
	
	$params = array($email,$password);

	if ( $oUser->doLogin($params) ) {
		echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
	} else {
		echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada no coinciden con nuestros registros.", "type" => "warning"));
	}

}

?>