<?php
header("access-control-allow-origin: *");
session_start(); 



// DataBase
include_once '../data/dataBase.php';
 

// Classes
include_once 'clases/cBanner.php';



$database = new Database();
$db = $database->getConnection();

$oBanner 	= new Banner($db);

$option = '';
$array[] = '';
$array2[] = '';
$files = '';

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

function generateRandomString($length = 3) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function sendMailBanner($params) {
	//session_destroy();
	
	$tiposBannerX = explode(",", $params[5]); 
	$tX = '';
	
	for($i=0; $i<count($tiposBannerX); $i++){
		
		//echo $tiposBannerX[$i] . ' ';
		if($tiposBannerX[$i] == 1){
			$tX .= ' La Prensa Grafica /';
		} else if($tiposBannerX[$i] == 2){
			$tX .= ' El Economista /';
		} else if($tiposBannerX[$i] == 3){
			$tX .= ' El Grafico /';
		}
	}
	
	//echo 'holis ' . date("l jS \of F Y", strtotime($_POST['fecha']));
	//Email information
	$admin_email = CORREOS;
	$admin_email .= $_POST['email'];
	$email = $params[3];
	$subject = 'Publicar banner para ' . $params[2];
	$comment = 'El ejecutivo de ventas ' . $params[2] . ' quiere publicar un '. $params[18] .' con el cliente ' . $params[4] . "\xA" .
				'No. Ticket: ' . $params[0] . "\xA" .
				'Codigo de Vendedor: ' . $params[1] . "\xA" .
				'Nombre: ' . $params[2] . "\xA" .
				'Email: ' . $params[3] . "\xA" .
				"------------------------------------------------- \xA" .
				'Cliente: ' . $params[4] . "\xA" .
				'Publicar en: ' . $tX . "\xA" .
				'Desde: ' . $params[6] . ' - Hasta: ' . $params[7] . "\xA" .
				'URL / Link Banner: ' . $params[8] . "\xA" .
				'Numero de impresiones: ' . $params[9] . "\xA" .
				'Inversion $USD: ' . $params[10] . "\xA" .
				'Mensaje: ' . $params[11] . "\xA" .
				"------------------------------------------------- \xA" .
				'TIENE ADJUNTO: ' . (isset($_SESSION['imgFile'])?"SI":"NO") . "\xA" .
				"------------------------------------------------- \xA" .
				'Solicitud enviada: ' . $params[13] . "\xA" .
				'IP: ' . $params[15] . "\xA"
				;
	  
	// //send email
	//echo $admin_email . $subject . $comment . $email;
	if(DEV != true){
		mail($admin_email, "$subject", $comment, "From:" . $email);
	}
	
}
// function sendMailBanner($params) {
	// session_destroy();
	// //echo 'holis ' . date("l jS \of F Y", strtotime($_POST['fecha']));
	// //Email information
	// $admin_email = "afigueroa@hashtag.sv" . ", ";
	// // $admin_email .= "aorellana@hashtag.sv" . ", ";
	// $admin_email .= $_POST['email'];
	// $email = $params[2];
	// $subject = 'Publicar banner para ' . $params[2];
	// $comment = 'El ejecutivo de ventas ' . $params[2] . ' quiere publicar un Banner con el cliente ' . $params[4] . "\xA" .
				// 'No. Ticket: ' . $params[0] . "\xA" .
				// 'Codigo de Vendedor: ' . $params[1] . "\xA" .
				// 'Nombre: ' . $params[2] . "\xA" .
				// 'Email: ' . $params[3] . "\xA" .
				// "------------------------------------------------- \xA" .
				// 'Cliente: ' . $params[4] . "\xA" .
				// 'Publicar en: ' . $params[5] . "\xA" .
				// 'Desde: ' . $params[6] . ' - Hasta: ' . $params[7] . "\xA" .
				// 'URL / Link Banner: ' . $params[8] . "\xA" .
				// 'Numero de impresiones: ' . $params[9] . "\xA" .
				// 'Inversion $USD: ' . $params[10] . "\xA" .
				// 'Mensaje: ' . $params[11] . "\xA" .
				// "------------------------------------------------- \xA" .
				// 'Solicitud enviada: ' . $params[13] . "\xA" .
				// 'IP: ' . $params[15] . "\xA"
				// ;
	  
	// // //send email
	// //mail($admin_email, "$subject", $comment, "From:" . $email);
	
// }

//Guarda Nuevo Usuario Web
if ( $option == 'nBanner' ) {
	try{
		//parametros 
		$trackid				= generateRandomString().'-'.generateRandomString().'-'.generateRandomString();
		$codeventas				= $_POST['code-ventas'];
		$nombre					= $_POST['first-name'];
		$email					= $_POST['email'];
		
		$cliente				= $_POST['cliente'];
		$publicacion			= '';
		for ($i=0; $i<count($_POST['publicacion']); $i++){
			$publicacion .= $_POST['publicacion'][$i].",";
		}
		// echo $publicacion;
		$fecha1 				= substr($_POST['single_cal3'], 0, 10); 
		$fecha2 				= substr($_POST['single_cal3'], 12); 
		$url					= $_POST['url'];
		
		$impresiones			= preg_replace('/[^0-9.]+/', '', $_POST['impresiones']);
		$impresiones			= (strlen($impresiones) <= 2) ? ($impresiones.'000') : $impresiones;
		//echo $impresiones;
		$inversion				= $_POST['inversion'];
		
		$message				= $_POST['message'];
		
		if(isset($_SESSION['imgFile'])){
			$files					= $_SESSION['imgFile'];
		}
		// echo 'aqui '.$files;
		// if (isset($_SESSION['imgFile'])){ 
			// echo "La sesión existe ...".$files; 
		// } else {
			// echo "La sesión no existe ...".$files; 
		// }
		
		
		$dt						= date("Y-m-d H:i:s");
		$lastchange				= date("Y-m-d H:i:s");
		
		$ip						= get_client_ip();
		
		$estadocita				= '1';
		$estado					= '1';
		
		$tipoBanner				= $_POST['tipobanner'];
		
		$params = array($trackid,$codeventas,$nombre,$email,$cliente,$publicacion,date('Y-m-d',strtotime(str_replace('-','/', $fecha1))),date('Y-m-d',strtotime(str_replace('-','/', $fecha2))),$url,$impresiones,$inversion,$message,$files,$dt,$lastchange,$ip,$estadocita,$estado,$tipoBanner);
		
		// for($i = 0; $i < count($params); $i++){
			// echo $params[$i];
		// }
		$save   = $oBanner->nuevo($params);
		
			if ( $save ) {
				// $log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
				// $oLog->setLog($log);
				sendMailBanner($params);
				// for($i = 0; $i < count($params); $i++){
					// echo $params[$i];
				// }
				session_destroy();
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
				
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//Guarda Nuevo Usuario Web
if ( $option == 'imgBanner' ) {
	try{
		
		//session_destroy();
		//session_start();
		
		for($i=0;$i<count($_FILES["file"]["name"]);$i++){
			

			$imgFile 			= $_FILES['file']['name'][$i];
			$tmp_dir 			= $_FILES['file']['tmp_name'][$i];
			$imgSize 			= $_FILES['file']['size'][$i];
			
			$upload_dir 		= '../images/files/'; // upload directory
			$imgExt 			= strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			
			//echo $imgExt;
			//$valid_extensions 	= array('jpeg', 'jpg', 'png', 'gif'); // valid extensions  
			
			$logo  		 		= "hashtag_" . time() . "." . $imgFile;
			
			// // // allow valid image file formats
			//if(in_array($imgExt, $valid_extensions)){
				// // Check file size '10MB'
				if($imgSize < 10000000){
					move_uploaded_file($tmp_dir,$upload_dir.$logo);
					
					 
					$array[] .= $logo;
					$array2[] .= $tmp_dir;
					
					$_SESSION['imgFile'] .= $logo.',';
					
					 
				}	else {
					$errMSG = "Sorry, your file is too large.";
				}
			//}	else {
				//$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			//}
			
			if(!isset($errMSG)) {
				//$business->businessLogo = "media/".$logo;
				echo json_encode(array("title" => "Success!", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
				exit(); 
			}

		
		}
		
		echo $_SESSION['imgFile'].'???';
		
		$_SESSION['imagenes'] = $array;
		$_SESSION['tmp_img'] = $array2;
		
		// for($k=0;$k<count($_SESSION['imagenes']);$k++){
			// echo 'chi '.$_SESSION['imagenes'][$k];
		// }
		
		// for($j=0;$j<count($_SESSION['tmp_img']);$j++){
			// echo 'no '.$_SESSION['tmp_img'][$j];
		// }
	
		//session_destroy();
		
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}


//actualizar banner aceptar
if ( $option == 'doneBanner' ) {
	try{
		//echo 'sdfsdf';
		//parametros 
		
		$idBanner					= $_POST['idBanner'];
		//echo $idBanner;
		
		$save   = $oBanner->update_aceptar_banner($idBanner,'2');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}


//actualizar cita cancelar
if ( $option == 'finBanner' ) {
	try{
		
		$idBanner					= $_POST['idBanner'];
		
		$save   = $oBanner->update_aceptar_banner($idBanner,'3');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar cita cancelar
if ( $option == 'cancelBanner' ) {
	try{
		
		$idBanner					= $_POST['idBanner'];
		
		$save   = $oBanner->update_aceptar_banner($idBanner,'4');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar si envio o no envio reporte de banner
if ( $option == 'enviarReporte' ) {
	try{
		
		$idBanner					= $_POST['idBanner'];
		$reporte					= $_POST['reporte'];
		
		$save   = $oBanner->update_reporte_banner($idBanner,$reporte);
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

?>