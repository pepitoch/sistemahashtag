<?php
	session_start();

	unset($_SESSION['iduser']);
	unset($_SESSION['name']);
	unset($_SESSION['email']);
	unset($_SESSION['idrol']);

  if(session_destroy()) {
    header("Location: ../admin/login.php");
  }
?>