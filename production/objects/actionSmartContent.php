<?php
header("access-control-allow-origin: *");
session_start(); 

date_default_timezone_set('America/el_salvador');

// DataBase
include_once '../data/dataBase.php';
 

// Classes
include_once 'clases/cSmartContent.php';



$database = new Database();
$db = $database->getConnection();

$oSmartContent 	= new SmartContent($db);

$option = '';
$array[] = '';
$array2[] = '';
$files = '';

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

function generateRandomString($length = 3) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}



function sendMailSmartContent($params) {
	
	$tipoRedX = explode(",", $params[6]); 
	$tX = '';
	
	for($i=0; $i<count($tipoRedX); $i++){
		
		
		if($tipoRedX[$i] == 1){
			$tX .= ' Facebook /';
		} else if($tipoRedX[$i] == 2){
			$tX .= ' Twitter /';
		} else if($tipoRedX[$i] == 3){
			$tX .= ' Instagram /';
		}
	}
	
	
	//Email information
	$headers = "From: " . $params[3] . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$headers .= "CC: " . $params[3] . "\r\n";
	
	$admin_email = CORREOS;
	
	$subject = 'Publicar SmartContent para ' . $params[2];
		
	$message = '<html><body>';
	
	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	$message .= "<tr style='background: #eee;'><td><strong>Ejecutivo:</strong> </td><td>" . strip_tags($params[2]) . "</td></tr>";
	$message .= "<tr><td><strong>Empresa:</strong> </td><td>" .strip_tags($params[4]) . "</td></tr>";
	
	$message .= "<tr><td><strong>No. Difusiones:</strong> </td><td>" . strip_tags($params[5]) . "</td></tr>";
	$message .= "<tr><td><strong>Difundir en:</strong> </td><td>" . strip_tags($tX) . "</td></tr>";
	$message .= "<tr><td><strong>Inversion:</strong> </td><td>$" . strip_tags($params[7]) . "</td></tr>";
	$message .= "<tr><td><strong>Fecha visita:</strong> </td><td>" . strip_tags($params[8]) . "</td></tr>";
	$message .= "<tr><td><strong>Fecha reportaje:</strong> </td><td>" . strip_tags($params[9]) . "</td></tr>";
	$message .= "<tr><td><strong>Fecha a publicar:</strong> </td><td>" . strip_tags($params[10]) . "</td></tr>";
	
	$message .= "<tr><td><strong>Observaciones:</strong> </td><td>" . htmlentities($params[11]) . "</td></tr>";
	$message .= "</table>";
	$message .= "<span></br>Enviado:".strip_tags($params[12]) ."</span>";
	$message .= "</body></html>";
	  
	
	if(DEV != true){
		mail($admin_email, "$subject", $message, $headers);
	}
	
}


//Guarda Nuevo Usuario Web
if ( $option == 'nSmartContent' ) {
	try{
		//parametros 
		$trackid				= generateRandomString().'-'.generateRandomString().'-'.generateRandomString();
		$codeventas				= $_POST['code-ventas'];
		$nombre					= $_POST['first-name'];
		$email					= $_POST['email'];
		
		$cliente				= $_POST['cliente'];
		$envio					= $_POST['envio'];
		
		$publicacion			= '';
		for ($i=0; $i<count($_POST['publicacion']); $i++){
			$publicacion .= $_POST['publicacion'][$i].",";
		}
		
		$inversion				= $_POST['inversion'];
		$fecha_visita 			= date('Y-m-d',strtotime(str_replace('-','/', $_POST['single_cal3'])));
		$fecha_reportaje 		= date('Y-m-d',strtotime(str_replace('-','/', $_POST['single_cal4'])));
		$fecha_publicacion		= date('Y-m-d',strtotime(str_replace('-','/', $_POST['single_cal5'])));
		$message 				= $_POST['message'];
				
				
		$fecha_ingreso			= date("Y-m-d H:i:s");
		$fecha_modificacion		= date("Y-m-d H:i:s");
		
		$estado					= '1';
		
		$params = array($trackid,$codeventas,$nombre,$email,$cliente,$envio,$publicacion,$inversion,$fecha_visita,$fecha_reportaje,$fecha_publicacion,$message,$fecha_ingreso,$fecha_modificacion,$estado);
		
		$save   = $oSmartContent->nuevo($params);
		
			if ( $save ) {
				
				sendMailSmartContent($params);
				
				echo json_encode(array("title" => "EXCELENTE", "text" => "SmartContent programado, a la brevedad se estaran confirmando las fechas de envio segun disponibilidad /", "type" => "success"));
				
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada. ", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. ", "type" => "error"));
	}
}


?>
				