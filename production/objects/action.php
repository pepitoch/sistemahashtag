<?php
header("access-control-allow-origin: *");

// DataBase
include_once '../data/dataBase.php';


// Classes
include_once 'clases/cCita.php';

$database = new Database();
$db = $database->getConnection();

$oCita 	= new Cita($db);

$option = '';

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}


function generateRandomString($length = 3) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


function sendMailCita($params) {
	//echo 'holis ' . date("l jS \of F Y", strtotime($_POST['fecha']));
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

	//Email information
	$admin_email = CORREOS;
	$admin_email .= $_POST['email'];
	$email = $params[2];
	$subject = 'Cita con ' . $params[1];
	$comment = 'El ejecutivo de ventas ' . $params[1] . ' quiere una cita con el cliente ' . $params[3] . "\xA" .
				'No. Ticket: ' . $params[0] . "\xA" .
				'Vendedor: ' . $params[1] . "\xA" .
				'Correo: ' . $params[2] . "\xA" . "\xA" .
				'Cliente: ' . $params[3] . "\xA" . "\xA" .
				'Historial: ' . $params[6] . "\xA" .
				'Mensaje: ' . $params[7] . "\xA" .
				//'Fecha y Hora: ' . date("l jS \of F Y", strtotime($_POST['fecha'])) . ' / ' . $params[5] . "\xA"
				'Fecha y Hora: ' . $dias[date('w', $params[4])].' '.date('d', $params[4]).' de '.$meses[date('n', $params[4])-1]. ' del '.date('Y', $params[4]) . ' / ' . $params[5] . "\xA"
				;
	  
	// //send email
	mail($admin_email, "$subject", $comment, "From:" . $email);
	
}

//Guarda Nuevo Usuario Web
if ( $option == 'nCita' ) {
	try{
		//parametros 
		$trackid				= generateRandomString().'-'.generateRandomString().'-'.generateRandomString();
		$nombre					= $_POST['first-name'];
		$email					= $_POST['email'];
		
		$cliente				= $_POST['cliente'];
		$fecha 					= strtotime ( $_POST['single_cal3'] ); 
		$hora					= $_POST['hora'];
		
		$historial				= $_POST['historial'];
		$message				= $_POST['message'];
		
		$dt						= date("Y-m-d H:i:s");
		$lastchange				= date("Y-m-d H:i:s");
		
		$ip						= get_client_ip();
		
		$estadocita				= '1';
		$estado					= '1';
		
		
		
		
		$params = array($trackid,$nombre,$email,$cliente,$fecha,$hora,$historial,$message,$dt,$lastchange,$ip,$estadocita,$estado);
		
		$save   = $oCita->nuevo($params);
		
			if ( $save ) {
				//$log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
				//$oLog->setLog($log);
				sendMailCita($params);
				// for($i = 0; $i < count($params); $i++){
					// echo $params[$i];
				// }
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar cita aceptar
if ( $option == 'doneCita' ) {
	try{
		//echo 'sdfsdf';
		//parametros 
		
		$idCita					= $_POST['idCita'];
		//echo $idCita;
		
		$save   = $oCita->update_aceptar_cita($idCita,'2');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar cita cancelar
if ( $option == 'finCita' ) {
	try{
		
		$idCita					= $_POST['idCita'];
		
		$save   = $oCita->update_aceptar_cita($idCita,'3');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar cita cancelar
if ( $option == 'cancelCita' ) {
	try{
		
		$idCita					= $_POST['idCita'];
		
		$save   = $oCita->update_aceptar_cita($idCita,'4');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}

//actualizar cita delete
if ( $option == 'deleteCita' ) {
	try{
		
		$idCita					= $_POST['idCita'];
		
		$save   = $oCita->update_delete_cita($idCita,'0');
		
			if ( $save ) {
				
				echo json_encode(array("title" => "Success", "text" => "ok", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada.", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada.", "type" => "error"));
	}
}




?>