<?php
class Anuncio{
	//Constructor
	private $conn; 
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	
	// get count anuncio premium
	function get_count_anunciop(){
		try{
			$query 	= "SELECT COUNT(*) AS premium FROM anuncio WHERE idtipo_anuncio = 2;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get count anuncio premium
	function get_count_anunciob(){
		try{
			$query 	= "SELECT COUNT(*) AS basic FROM anuncio WHERE idtipo_anuncio = 1;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get count anuncio all
	function get_count_anuncio(){
		try{
			$query 	= "SELECT COUNT(*) FROM anuncio;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	function nuevo($params){
		try
		{
			$sql = "INSERT INTO anuncio (codigo,codigo_vendedor,nombreV,correoV,nombre,descripcion,imagen,repuesta_correo,fecha_ingreso,fecha_inicio,fecha_fin,estado,idtipo_anuncio,idformulario,frase,web,facebook,twitter,youtube) 
					VALUES (:codigo,:codigo_vendedor,:nombreV,:correoV,:nombre,:descripcion,:imagen,:repuesta_correo,:fecha_ingreso,:fecha_inicio,:fecha_fin,:estado,:idtipo_anuncio,:idformulario,:frase,:web,:facebook,:twitter,:youtube);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":codigo", $params[0]);
			$stmt->bindParam(":codigo_vendedor", $params[1]);
			$stmt->bindParam(":nombreV", $params[2]);
			$stmt->bindParam(":correoV", $params[3]);
			$stmt->bindParam(":nombre", $params[4]);
			$stmt->bindParam(":descripcion", $params[5]);
			$stmt->bindParam(":imagen", $params[6]);
			$stmt->bindParam(":repuesta_correo", $params[7]);
			$stmt->bindParam(":fecha_ingreso", $params[8]);
			$stmt->bindParam(":fecha_inicio", $params[9]);
			$stmt->bindParam(":fecha_fin", $params[10]);
			$stmt->bindParam(":estado", $params[11]);
			$stmt->bindParam(":idtipo_anuncio", $params[12]);
			$stmt->bindParam(":idformulario", $params[13]);
			//premium
			$stmt->bindParam(":frase", $params[14]);
			$stmt->bindParam(":web", $params[15]);
			$stmt->bindParam(":facebook", $params[16]);
			$stmt->bindParam(":twitter", $params[17]);
			$stmt->bindParam(":youtube", $params[18]);
				
			if ($stmt->execute()){
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	// get anuncio all
	function get_anuncio_all(){
		try{
			//$query 	= "SELECT * FROM anuncio WHERE estado = 1 AND fecha_ingreso >= '".date('Y-m-d', strtotime('first day of this month'))."';";
			$query 	= "SELECT * FROM anuncio ORDER BY fecha_ingreso DESC;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	// get date detail
	function get_anuncio_detail($idanuncio){
		try{
			$query 	= "SELECT * FROM anuncio WHERE idanuncio = :idanuncio;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idanuncio", $idanuncio);
			$stmt->execute();
            
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	
	//actualizar anucio que ya fue publicado aceptar
	function update_estado_anuncio($idanuncio){
		try
		{
			// query to insert record
			$query = "UPDATE anuncio SET estado=1, fecha_inicio=:fecha_inicio WHERE idanuncio=:idanuncio;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":idanuncio", $idanuncio);
			$stmt->bindParam(":fecha_inicio", date("Y-m-d H:i:s"));

			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>