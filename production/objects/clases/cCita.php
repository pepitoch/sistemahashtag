<?php
class Cita{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	
	function nuevo($params){
		try
		{
			$sql = "INSERT INTO cita (codigo,nombre,correo,nombre_cliente,fecha_cita,hora_cita,historial,comentario,fecha_creacion,fecha_modificacion,ip,estado_cita,estado) 
					VALUES (:codigo,:nombre,:correo,:nombre_cliente,:fecha_cita,:hora_cita,:historial,:comentario,:fecha_creacion,:fecha_modificacion,:ip,:estado_cita,:estado);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":codigo", $params[0]);
			$stmt->bindParam(":nombre", $params[1]);
			$stmt->bindParam(":correo", $params[2]);
			$stmt->bindParam(":nombre_cliente", $params[3]);
			$stmt->bindParam(":fecha_cita", $params[4]);
			$stmt->bindParam(":hora_cita", $params[5]);
			$stmt->bindParam(":historial", $params[6]);
			$stmt->bindParam(":comentario", $params[7]);
			$stmt->bindParam(":fecha_creacion", $params[8]);
			$stmt->bindParam(":fecha_modificacion", $params[9]);
			$stmt->bindParam(":ip", $params[10]);
			$stmt->bindParam(":estado_cita", $params[11]);
			$stmt->bindParam(":estado", $params[12]);
				
			if ($stmt->execute()){
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	
	// get date manana
	function get_date(){
		try{
			$query 	= "SELECT idcita,nombre,fecha_cita,hora_cita,estado_cita FROM cita WHERE hora_cita LIKE '%AM%' AND fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."' AND estado_cita = '1' ORDER BY fecha_cita LIMIT 5;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get date tarde
	function get_date_t(){
		try{
			$query 	= "SELECT idcita,nombre,fecha_cita,hora_cita,estado_cita FROM cita WHERE hora_cita LIKE '%PM%' AND fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."' AND estado_cita = '1' ORDER BY fecha_cita LIMIT 5;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get date all
	function get_date_all(){
		try{
			$query 	= "SELECT * FROM cita WHERE estado='1' ORDER BY fecha_cita DESC;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get date all
	function get_date_detail($idcita){
		try{
			$query 	= "SELECT * FROM cita WHERE idcita = :idcita;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idcita", $idcita);
			$stmt->execute();
            
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	//actualizar cita aceptar
	function update_aceptar_cita($idCita,$idestado){
		try
		{
			// query to insert record
			$query = "UPDATE cita SET estado_cita=:idestado, fecha_modificacion=:fecha_modificacion WHERE idcita=:idcita;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":idcita", $idCita);
			$stmt->bindParam(":idestado", $idestado);
			$stmt->bindParam(":fecha_modificacion", date("Y-m-d H:i:s"));

			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	//actualizar cita aceptar
	function update_delete_cita($idCita,$idestado){
		try
		{
			// query to insert record
			$query = "UPDATE cita SET estado=:idestado, fecha_modificacion=:fecha_modificacion WHERE idcita=:idcita;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":idcita", $idCita);
			$stmt->bindParam(":idestado", $idestado);
			$stmt->bindParam(":fecha_modificacion", date("Y-m-d H:i:s"));

			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>