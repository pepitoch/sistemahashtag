<?php
class Consumo{
	
	function getConsumo($url){
	
		$headers = array( 		
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		if (FALSE === $ch)
		throw new Exception('failed to initialize');
		
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_HTTPGET, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		// Execute post
		$result = curl_exec($ch);
		
		if (FALSE === $result)
		 throw new Exception(curl_error($ch), curl_errno($ch));
		// Close connection
		curl_close($ch);
		return $result;
	}
	
	function postConsumo($url,$body){
	
		$headers = array( 		
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		if (FALSE === $ch)
		throw new Exception('failed to initialize');
		
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS,  $body  );
		// Execute post
		$result = curl_exec($ch);
		
		if (FALSE === $result)
		 throw new Exception(curl_error($ch), curl_errno($ch));
		// Close connection
		curl_close($ch);
		return $result;
	}
	
	function putConsumo($url,$body){
	
		$headers = array( 		
			"cache-control: no-cache",
			"content-type: application/json"
		);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PUT",
		  CURLOPT_POSTFIELDS => $body,
		  CURLOPT_HTTPHEADER => $headers,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
}
?>