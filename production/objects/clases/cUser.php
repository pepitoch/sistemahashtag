<?php
session_start();

class User{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	
	public function doLogin($params) {
		try
		{
			// query to insert record
			$query = "SELECT * FROM user 
						WHERE email = :email AND password = :password AND estado = 1";

			// prepare query
			$stmt = $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":email", $params[0]);
			$stmt->bindParam(":password", sha1($params[1]));
			
			
			// execute query
			$stmt->execute();
			$userRow = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() == 1) {
				
				$_SESSION['iduser']     = $userRow['iduser'];
				$_SESSION['name']   	= $userRow['name'];
				$_SESSION['email'] 		= $userRow['email'];
				$_SESSION['idrol']     	= $userRow['idrol'];
				
				return true;
				
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}

	}
	
	
}
?>