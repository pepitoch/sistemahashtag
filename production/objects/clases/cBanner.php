<?php
class Banner{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
    // function __construct() 
    // {
        // global $DATA;
        // $this->DATA = $DATA;
    // }
	
	// function nuevo($params){
		// $sql = "INSERT INTO cita (codigo,nombre,correo,nombre_cliente,fecha_cita,hora_cita,historial,comentario,fecha_creacion,fecha_modificacion,ip,estado_cita,estado) 
				// VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
		// $save = $this->DATA->Execute($sql, $params);
		// if ($save){
			// return true; 
		// } else {
			// return false;
		// }
	// }
	
	function nuevo($params){
		try
		{
			$sql = "INSERT INTO banner (codigo,codigo_vendedor,nombre,correo,nombre_cliente,idtipo_publicacion,inicio_fecha,fin_fecha,url,numero_impresion,inversion,mensaje,files,fecha_creacion,fecha_modificacion,ip,estado_banner,estado,tipo_banner) 
					VALUES (:codigo,:codigo_vendedor,:nombre,:correo,:nombre_cliente,:tipo_publicacion,:inicio_fecha,:fin_fecha,:url,:numero_impresion,:inversion,:mensaje,:files,:fecha_creacion,:fecha_modificacion,:ip,:estado_banner,:estado,:tipo_banner);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":codigo", $params[0]);
			$stmt->bindParam(":codigo_vendedor", $params[1]);
			$stmt->bindParam(":nombre", $params[2]);
			$stmt->bindParam(":correo", $params[3]);
			$stmt->bindParam(":nombre_cliente", $params[4]);
			$stmt->bindParam(":tipo_publicacion", $params[5]);
			$stmt->bindParam(":inicio_fecha", $params[6]);
			$stmt->bindParam(":fin_fecha", $params[7]);
			$stmt->bindParam(":url", $params[8]);
			$stmt->bindParam(":numero_impresion", $params[9]);
			$stmt->bindParam(":inversion", $params[10]);
			$stmt->bindParam(":mensaje", $params[11]);
			$stmt->bindParam(":files", $params[12]);
			$stmt->bindParam(":fecha_creacion", $params[13]);
			$stmt->bindParam(":fecha_modificacion", $params[14]);
			$stmt->bindParam(":ip", $params[15]);
			$stmt->bindParam(":estado_banner", $params[16]);
			$stmt->bindParam(":estado", $params[17]);
			$stmt->bindParam(":tipo_banner", $params[18]);
				
			if ($stmt->execute()){
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	
	// get banner all
	function get_banner_all(){
		try{
			$query 	= "SELECT * FROM banner WHERE fecha_creacion >= '".date('Y-m-d', strtotime('-3 months first day of this month'))."' AND (estado_banner = '1' || estado_banner = '2' || estado_banner = '3') ORDER BY inicio_fecha DESC";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	
	// get date detail
	function get_banner_detail($idbanner){
		try{
			$query 	= "SELECT * FROM banner WHERE idbanner = :idbanner;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idbanner", $idbanner);
			$stmt->execute();
            
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	//actualizar banner aceptar
	function update_aceptar_banner($idBanner,$idestado){
		try
		{
			// query to insert record
			$query = "UPDATE banner SET estado_banner=:idestado, fecha_modificacion=:fecha_modificacion WHERE idbanner=:idbanner;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":idbanner", $idBanner);
			$stmt->bindParam(":idestado", $idestado);
			$stmt->bindParam(":fecha_modificacion", date("Y-m-d H:i:s"));

			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	//actualizar banner aceptar
	function update_reporte_banner($idBanner,$reporte){
		try
		{
			// query to insert record
			$query = "UPDATE banner SET reporte=:reporte WHERE idbanner=:idbanner;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":idbanner", $idBanner);
			$stmt->bindParam(":reporte", $reporte);
			
			
			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>