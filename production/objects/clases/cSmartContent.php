<?php
class SmartContent{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
    
	function nuevo($params){
		try
		{
			$sql = "INSERT INTO smartcontent (codigo,codigo_vendedor,nombreV,correoV,nombre_empresa,difusiones,red_social,inversion,fecha_visita,fecha_reportaje,fecha_publicacion,mensaje,fecha_ingreso,fecha_modificacion,estado) 
					VALUES (:codigo,:codigo_vendedor,:nombreV,:correoV,:nombre_empresa,:difusiones,:red_social,:inversion,:fecha_visita,:fecha_reportaje,:fecha_publicacion,:mensaje,:fecha_ingreso,:fecha_modificacion,:estado);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":codigo", $params[0]);
			$stmt->bindParam(":codigo_vendedor", $params[1]);
			$stmt->bindParam(":nombreV", $params[2]);
			$stmt->bindParam(":correoV", $params[3]);
			$stmt->bindParam(":nombre_empresa", $params[4]);
			$stmt->bindParam(":difusiones", $params[5]);
			$stmt->bindParam(":red_social", $params[6]);
			$stmt->bindParam(":inversion", $params[7]);
			$stmt->bindParam(":fecha_visita", $params[8]);
			$stmt->bindParam(":fecha_reportaje", $params[9]);
			$stmt->bindParam(":fecha_publicacion", $params[10]);
			$stmt->bindParam(":mensaje", $params[11]);
			$stmt->bindParam(":fecha_ingreso", $params[12]);
			$stmt->bindParam(":fecha_modificacion", $params[13]);
			$stmt->bindParam(":estado", $params[14]);
				
			if ($stmt->execute()){
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	
	// get banner all
	function get_smart_all(){
		try{
			$query 	= "SELECT * FROM smartcontent WHERE fecha_ingreso >= '".date('Y-m-d', strtotime('-3 months first day of this month'))."' ORDER BY fecha_ingreso DESC";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	// get date detail
	function get_smart_detail($idsmart){
		try{
			$query 	= "SELECT * FROM smartcontent WHERE idsmartcontent = :idsmart;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idsmart", $idsmart);
			$stmt->execute();
            
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	
}
?>