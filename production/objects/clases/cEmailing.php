<?php
class Emailing{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
    
	function nuevo($params){
		try
		{
			$sql = "INSERT INTO emailing (codigo,codigo_vendedor,nombreV,correoV,nombre_empresa,asunto,correo_remitente,url,imagen,envios,inversion,fecha,mensaje,fecha_ingreso,fecha_modificacion,estado) 
					VALUES (:codigo,:codigo_vendedor,:nombreV,:correoV,:nombre_empresa,:asunto,:correo_remitente,:url,:imagen,:envios,:inversion,:fecha,:mensaje,:fecha_ingreso,:fecha_modificacion,:estado);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":codigo", $params[0]);
			$stmt->bindParam(":codigo_vendedor", $params[1]);
			$stmt->bindParam(":nombreV", $params[2]);
			$stmt->bindParam(":correoV", $params[3]);
			$stmt->bindParam(":nombre_empresa", $params[4]);
			$stmt->bindParam(":asunto", $params[5]);
			$stmt->bindParam(":correo_remitente", $params[6]);
			$stmt->bindParam(":url", $params[7]);
			$stmt->bindParam(":imagen", $params[8]);
			$stmt->bindParam(":envios", $params[9]);
			$stmt->bindParam(":inversion", $params[10]);
			$stmt->bindParam(":fecha", $params[11]);
			$stmt->bindParam(":mensaje", $params[12]);
			$stmt->bindParam(":fecha_ingreso", $params[13]);
			$stmt->bindParam(":fecha_modificacion", $params[14]);
			$stmt->bindParam(":estado", $params[15]);
				
			if ($stmt->execute()){
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	
	// get banner all
	function get_emailing_all(){
		try{
			$query 	= "SELECT * FROM emailing WHERE fecha_ingreso >= '".date('Y-m-d', strtotime('-3 months first day of this month'))."' ORDER BY fecha_ingreso DESC";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	// get date detail
	function get_emailing_detail($idemailing){
		try{
			$query 	= "SELECT * FROM emailing WHERE idemailing = :idemailing;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idemailing", $idemailing);
			$stmt->execute();
            
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	function nuevo_fecha($params){
		try
		{
			$sql = "INSERT INTO emailing_fecha_envio (fecha,correlativo,fecha_ingreso,idemailing) 
					VALUES (:fecha,:correlativo,:fecha_ingreso,:idemailing);";
			
			// prepare query
			$stmt = $this->conn->prepare($sql);
			
			
			// bind values
			$stmt->bindParam(":fecha", $params[1]);
			$stmt->bindParam(":correlativo", $params[2]);
			$stmt->bindParam(":fecha_ingreso", $params[3]);
			$stmt->bindParam(":idemailing", $params[0]);
			
				
			if ($stmt->execute()){
				
				return true; 
			} else {
				return false;
			}
			
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	//actualizar cita aceptar
	public function update_fecha_emailing($params){
		try
		{
			// query to insert record
			$query = "UPDATE emailing_fecha_envio SET fecha=:fecha, fecha_ingreso=:fecha_ingreso WHERE idemailing_fecha_envio=:idemailing_fecha_envio;";

			// prepare query
			$stmt = $this->conn->prepare($query);

			// bind values
			$stmt->bindParam(":fecha", $params[1]);
			$stmt->bindParam(":fecha_ingreso", $params[3]);
			$stmt->bindParam(":idemailing_fecha_envio",  $params[4]);

			// execute query
			if($stmt->execute()){
				if ( $stmt->rowCount() > 0 ) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
		
	
	// verificar si existe el correlativo en la tabla de fechas programadas para emailing
	public function verificar_fecha($params) {
		try
		{
			// query to insert record
			$query = "SELECT * FROM emailing_fecha_envio WHERE correlativo = :correlativo AND idemailing = :idemailing;";

			// prepare query
			$stmt = $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":correlativo", $params[2]);
			$stmt->bindParam(":idemailing", $params[0]);
			
			// execute query
			$stmt->execute();
			$userRow = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() == 1) {
				
				//actualizar fecha de cada envio
				array_push($params, $userRow['idemailing_fecha_envio']);
				$this->update_fecha_emailing($params);
				//echo 'no';
				return true;
				
			}else if ($stmt->rowCount() == 0) {
				//echo 'chi';
				$this->nuevo_fecha($params);
				return true;
			} else {
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}

	}
	
	
	
	
	// get emailing de envios
	function get_emailing_fecha($idemailing){
		try{
			$query 	= "SELECT * FROM emailing_fecha_envio WHERE idemailing = $idemailing ORDER BY correlativo";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	
	// get numeros de emailing de envios
	function get_count_emailing_envio($idemailing){
		try{
			$query 	= "SELECT COUNT(*) AS nenvios FROM emailing_fecha_envio WHERE idemailing = $idemailing;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
	// get date detail
	function get_fecha_asignada($idemailing,$correlativo){
		try{
			$query 	= "SELECT fecha FROM emailing_fecha_envio WHERE idemailing = :idemailing AND correlativo = :correlativo;";
			
			$stmt = $this->conn->prepare( $query );
			// bind values
			$stmt->bindParam(":idemailing", $idemailing);
			$stmt->bindParam(":correlativo", $correlativo);
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	
}
?>