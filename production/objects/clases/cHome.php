<?php
class Home{
	//Constructor
	private $conn;
	
	// constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	
	// get date manana
	function get_count_citas(){
		try{
			$query 	= "SELECT COUNT(*) AS ncitas FROM cita WHERE fecha_cita >= '".strtotime(date('Y-m-d', strtotime('monday this week')))."';"; 
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get date tarde
	function get_count_banner(){
		try{
			$query 	= "SELECT COUNT(*) AS nbanner FROM banner WHERE fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."';";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get number anuncios
	function get_count_anuncio(){
		try{
			$query 	= "SELECT COUNT(*) AS nanuncio FROM anuncio WHERE fecha_ingreso >= '".date('Y-m-d', strtotime('monday this week'))."';";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchColumn();
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	// get vendedores all abnner
	function get_banner_vendedor(){
		try{
			$query 	= "select nombre, codigo_vendedor, count(codigo_vendedor) as total
					from banner 
					where fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."'
					AND (estado_banner = 1 || estado_banner = 2 || estado_banner = 3) 
					group by nombre 
					order by total desc LIMIT 10;";
			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
            
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			
			return $results;
			
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
}
?>