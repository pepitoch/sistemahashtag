<?php
header("access-control-allow-origin: *");
session_start(); 

date_default_timezone_set('America/el_salvador');

// DataBase
include_once '../data/dataBase.php';
 

// Classes
include_once 'clases/cEmailing.php';



$database = new Database();
$db = $database->getConnection();

$oEmailing 	= new Emailing($db);

$option = '';
$array[] = '';
$array2[] = '';
$files = '';

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

function generateRandomString($length = 3) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}



function sendMailEmailing($params) {
	
	//Email information
	$headers = "From: " . $params[3] . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$headers .= "CC: " . $params[3] . "\r\n";
	
	$admin_email = CORREOS;
	
	$subject = 'Publicar Campana Emailing para ' . $params[2];
		
	$message = '<html><body>';
	
	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	$message .= "<tr style='background: #eee;'><td><strong>Ejecutivo:</strong> </td><td>" . strip_tags($params[2]) . "</td></tr>";
	$message .= "<tr><td><strong>Empresa:</strong> </td><td>" .strip_tags($params[4]) . "</td></tr>";
	$message .= "<tr><td><strong>Asunto:</strong> </td><td>" . strip_tags($params[5]) . "</td></tr>";
	$message .= "<tr><td><strong>Remitente:</strong> </td><td>" . strip_tags($params[6]) . "</td></tr>";
	$message .= "<tr><td><strong>URL Campana:</strong> </td><td>" . strip_tags($params[7]) . "</td></tr>";
	$message .= "<tr><td><strong>URL Imagen:</strong> </td><td>" . URL_HT.substr($params[8], 3) . "</td></tr>";
	$message .= "<tr><td><strong>No. Envios:</strong> </td><td>" . strip_tags($params[9]) . "</td></tr>";
	$message .= "<tr><td><strong>Inversion:</strong> </td><td>$" . strip_tags($params[10]) . "</td></tr>";
	$message .= "<tr><td><strong>Fecha tentativa:</strong> </td><td>" . strip_tags($params[11]) . "</td></tr>";
	
	$message .= "<tr><td><strong>Observaciones:</strong> </td><td>" . htmlentities($params[12]) . "</td></tr>";
	$message .= "</table>";
	$message .= "<span></br>Enviado:".strip_tags($params[13]) ."</span>";
	$message .= "</body></html>";
	  
	
	if(DEV != true){
		mail($admin_email, "$subject", $message, $headers);
	}
	
}


//Guarda Nuevo Usuario Web
if ( $option == 'nEmailing' ) {
	try{
		//parametros 
		$trackid				= generateRandomString().'-'.generateRandomString().'-'.generateRandomString();
		$codeventas				= $_POST['code-ventas'];
		$nombre					= $_POST['first-name'];
		$email					= $_POST['email'];
		
		$cliente				= $_POST['cliente'];
		$asunto					= $_POST['asunto'];
		$remitente				= $_POST['remitente'];
		$url					= $_POST['url'];
		$envio					= $_POST['envio'];
		$inversion				= $_POST['inversion'];
		$fecha 					= date('Y-m-d',strtotime(str_replace('-','/', $_POST['single_cal3'])));
		$message 				= $_POST['message'];
		
		if($_FILES['filelife']['name']!=NULL){
			$imgFile = $_FILES['filelife']['name'];
			$tmp_dir = $_FILES['filelife']['tmp_name'];
			$imgSize = $_FILES['filelife']['size'];
			$upload_dir = '../images/emailing/'; // upload directory						
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
					// valid image extensions

			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'pdf', 'docx'); // valid extensions
			// rename uploading image
			$productpic = "emailing_".$remitente . "_" . time() . "." . $imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// Check file size '5MB'
				if($imgSize < 5000000){
					
					move_uploaded_file($tmp_dir,$upload_dir.$productpic);
					
					$errorMsg = 'Imagen subida correctamente para campana emailing';
					
				}	else {
					//echo "Lo sentimos, archivo muy pesado.";
					$errorMsg = 'Archivo excede los 5 Megas';
				}
			}	else {
				
				//echo "Lo sentimos, solo JPG, JPEG, PNG & GIF tipo de archivos soportados.";
				$errorMsg = 'Los archivos permitidos son imagenes, PDF y documentos tipo docx';
			}
		}
		
				
		$fecha_ingreso			= date("Y-m-d H:i:s");
		$fecha_modificacion		= date("Y-m-d H:i:s");
		
		$estado					= '1';
		$params = array($trackid,$codeventas,$nombre,$email,$cliente,$asunto,$remitente,$url,$upload_dir.$productpic,$envio,$inversion,$fecha,$message,$fecha_ingreso,$fecha_modificacion,$estado);
		
		$save   = $oEmailing->nuevo($params);
		
			if ( $save ) {
				
				sendMailEmailing($params);
				
				echo json_encode(array("title" => "EXCELENTE", "text" => "Emailing programado, a la brevedad se estaran confirmando las fechas de envio segun disponibilidad / $errorMsg", "type" => "success"));
				
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada. $errorMsg", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. $errorMsg", "type" => "error"));
	}
}


//Guarda Nuevo date de emailing
if ( $option == 'addDates' ) {
	try{
		//parametros 
		$id_emailing			= $_POST['idEmailing'];
		$fecha					= date('Y-m-d H:i:s',strtotime($_POST['fecha']));
		$correlativo			= $_POST['correlativo'];
			
		$fecha_ingreso			= date("Y-m-d H:i:s");
		
		$params = array($id_emailing,$fecha,$correlativo,$fecha_ingreso);
		
		$save   = $oEmailing->verificar_fecha($params);
		
			if ( $save ) {
												
				echo json_encode(array("title" => "EXCELENTE", "text" => "Fecha agregada", "type" => "success"));
				
			} else {
				echo json_encode(array("title" => "Warning", "text" => "Revisa la informacion proporcionada. ", "type" => "warning"));
			}
			
	}catch (Exception $e){
		echo json_encode(array("title" => "Error", "text" => "Revisa la informacion proporcionada. ", "type" => "error"));
	}
}


?>
				