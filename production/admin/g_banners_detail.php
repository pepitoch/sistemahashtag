<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cBanner.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oBanner = new Banner($db);
	
	if(isset($_GET['idbanner'])){
		$vBanner 		= $oBanner->get_banner_detail($_GET['idbanner']);
		if($vBanner){
			$ticket			= $vBanner['codigo'];
			$cod_vendedor	= $vBanner['codigo_vendedor'];
			$nombre			= $vBanner['nombre'];
			$correo			= $vBanner['correo'];
			$cliente		= $vBanner['nombre_cliente'];
			$tipo_pub		= $vBanner['idtipo_publicacion'];
			$fecha_i		= $vBanner['inicio_fecha'];
			$fecha_f		= $vBanner['fin_fecha'];
			$url			= $vBanner['url'];
			$n_impresion    = $vBanner['numero_impresion'];
			$inversion		= $vBanner['inversion'];
			$comentario		= $vBanner['mensaje'];
			$files			= $vBanner['files'];
			$creado			= $vBanner['fecha_creacion'];
			$ip				= $vBanner['ip'];
			$estado_banner	= $vBanner['estado_banner'];
			$tipo_banner	= $vBanner['tipo_banner'];
		}
	} else {
		header("Location: g_banners.php"); /* Redirect browser */
		exit();
	}
	
	function number_to_money($value, $symbol = '$', $decimals = 2)
	{
		return $symbol . ($value < 0 ? '-' : '') . number_format(abs($value), $decimals);
	}
	
	// // setlocale(LC_ALL,"es_ES");
	// // setlocale(LC_MONETARY, 'en_US');
					
	// $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	// $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
			<div class="ht-back">			
				<a onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i> Atr&aacute;s</a>
			</div>
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banners Detalle <small></small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1><?=$tipo_banner?>: <?=$cliente?></h1>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <ul class="stats-overview ht-stats-overview-3">
                        <li>
                          <span class="fa fa-globe fa-4x"></span>
                          <h3 class=""> <?=$n_impresion;?> </h3>
                          <span class="value text-success"> Impresiones </span>
                        </li>
                        <li>
                          <span class="fa fa-usd fa-4x"></span>
                          <h3 class=""> <?=number_to_money($inversion);?> </h3>
                          <span class="value text-success"> +IVA </span>
                        </li>
                        <li class="last-li">
                          <span class="fa fa-calendar fa-4x"></span>
                          <h3 class=""> <?=date('d-m-Y',strtotime(str_replace('-','/', $fecha_i)));?> - <?=date('d-m-Y',strtotime(str_replace('-','/', $fecha_f)));?> </h3>
                          <span class="value text-success"> Inicio - Fin </span>
                        </li>
                      </ul>
                      <br />

					<!-- start project-detail sidebar -->
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <section class="panel">

                        <div class="x_title">
                          <h2>Detalles</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                          <h3 class="green"><i class="fa fa-paint-brush"></i> No. Ticket: <?=$ticket?></h3>
							<p><?=$comentario?></p>
                          <br />
						  
							<div class="col-md-8 col-sm-12 col-xs-12">

							  <div class="project_detail">

								<p class="title">Cliente / Empresa</p>
								<p><?=$cliente?></p>
								<p class="title">URL / Link</p>
								<p><?=$url?></p>
								<p class="title">Plataforma de Publicacion</p>
								<p>
									<?php
										$tiposBannerX = explode(",", $tipo_pub);
										$tX = '';
										
										for($i=0; $i<count($tiposBannerX); $i++){
											
											if($tiposBannerX[$i] != ''){
												
												if($tiposBannerX[$i] == 1){
													$tX .= ' La Prensa Grafica /';
												} else if($tiposBannerX[$i] == 2){
													$tX .= ' El Economista /';
												} else if($tiposBannerX[$i] == 3){
													$tX .= ' El Grafico /';
												}
											}
										}
										
										echo substr($tX, 0, -1);
									
									?>
								</p>
								<hr/>
								<p class="title">Codigo Ejecutivo de Ventas</p>
								<p><?=$cod_vendedor?></p>
								<p class="title">Ejecutivo de Ventas</p>
								<p><?=$nombre?></p>
								<p class="title">Correo Electronico</p>
								<p><?=$correo?></p>
							  </div>
							  
							
							
							

							  <hr/>
							  <div class="project_detail">

								<p class="title">Solicitud Creada</p>
								<p><?=$creado?></p>
								<p class="title">IP</p>
								<p><?=$ip?></p>
							  </div>
							
							</div>
							
							
							<div class="col-md-4 col-sm-12 col-xs-12">
								
								<h4>Archivos</h4>
								<ul class="list-unstyled project_files ht-projects-files">
									<?php
										$imgFiles = explode(",", $files);
										$tX = '';
										
										for($j=0; $j<count($imgFiles); $j++){
											
											if($imgFiles[$j] != ''){
												echo '<li><a target="_blank" href="../images/files/' . $imgFiles[$j] .
												'"><i class="fa fa-picture-o"></i> ' . $imgFiles[$j] . '</a></li>';
											}
											
										}
										
										
									
									?>
									
								</ul>
							</div>
							
                        </div>

                      </section>
					 </div>
                    <!-- end project-detail sidebar -->
					  
					<!-- start project-detail sidebar -->
                    <div class="col-md-4 col-sm-12 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>Progreso</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
							<p id="ht-text-banner">
								<?php
									if($estado_banner == 1){
										echo 'Banner pendiente por aceptar';
									} else if($estado_banner == 2){
										echo 'Banner aceptado y en desarrollo';
									} else if($estado_banner == 3){
										echo 'Banner publicado';
									} else if($estado_banner == 4){
										echo 'Banner cancelado';
									} else {
										echo 'No hay datos de progreso';
									}
								?>
							</p>
							<div class="project_progress">
								<?php
									if($estado_banner == 1){
										
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-orange ht-progress" role="progressbar" data-transitiongoal="25"></div>
								</div>
								<small class="ht-progress-text">25% Completo</small>
								<?php
									} else if($estado_banner == 2){
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-green ht-progress" role="progressbar" data-transitiongoal="75"></div>
								</div>
								<small class="ht-progress-text">75% Completo</small>
								<?php
									} else if($estado_banner == 3){
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-blue-sky ht-progress" role="progressbar" data-transitiongoal="100"></div>
								</div>
								<small class="ht-progress-text">100% Completo</small>
								<?php
									} else if($estado_banner == 4){
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-red ht-progress" role="progressbar" data-transitiongoal="100"></div>
								</div>
								<small class="ht-progress-text">100% Completo</small>
								<?php
									}
								?>
							</div>
                          <br />

                          <div class="text-left mtop20">
							<?php
								if($estado_banner == 1){
									echo '<a href="#" class="btn btn-sm btn-success btn-block" id="ht-aceptar">Aceptar</a><br/>
											<a href="#" class="btn btn-sm btn-info btn-block" id="ht-finalizar">Publicado</a><br/>
											<a href="#" class="btn btn-sm btn-danger btn-block" id="ht-cancelar">Cancelar</a>';
								} else if($estado_banner == 2){
									echo '<a href="#" class="btn btn-sm btn-info btn-block" id="ht-finalizar">Publicado</a><br/>
											<a href="#" class="btn btn-sm btn-danger btn-block" id="ht-cancelar">Cancelar</a>';
								} else {
									echo '';
								}
							?>
                            
                          </div>
                        </div>

                      </section>

                    </div>
                    <!-- end project-detail sidebar -->


                    </div>

                    

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
	<script>
		//$(document).ready(function() {
			var id_banner = <?=$_GET['idbanner'];?>;
			
			//aceptar
			$( "#ht-aceptar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'doneBanner',idBanner:id_banner},
				  url: "../objects/actionBanner.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Banner aceptado',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-banner').text('Banner aceptado segun la fecha');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'75',
						  'style':'width: 75%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-green');
					  $('.ht-progress-text').text('75% Completo');
					  $('#ht-aceptar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			//finalizar
			$( "#ht-finalizar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'finBanner',idBanner:id_banner},
				  url: "../objects/actionBanner.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Banner finalizada',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-banner').text('Banner cerrada, ya se realizo!');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass();
					  $('.progress div').addClass('progress-bar bg-blue-sky');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			
			//cancelar
			$( "#ht-cancelar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'cancelBanner',idBanner:id_banner},
				  url: "../objects/actionBanner.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Banner cancelado',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-banner').text('Banner cancelado');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-red');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
		// });
	</script>

  </body>
</html>