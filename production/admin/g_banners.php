<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cBanner.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oBanner = new Banner($db);
	
	
	$vBanner 		= $oBanner->get_banner_all();
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banners <small>Lista de banners</small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Banners</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>&nbsp;</p>

                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-hover table-bordered dt-responsive display nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          
                          
                          <th >Ejecutivo Ventas</th>
                          <th >Nombre Cliente</th>
                          <th>Fecha CREACION TICKET</th>
                          <th >Fechas INICIO</th>
                          
						  <th >Estado</th>
                          <th >#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php
					
						if($vBanner){
							foreach ($vBanner AS $id => $arrBanner) {
						?>
                        <tr>
                          
                          
                          <td>
                            <a><?=$arrBanner['nombre'];?></a>
                            <br />
                            <small><?=$arrBanner['correo'];?></small>
                          </td>
                          <td><?=$arrBanner['nombre_cliente'];?></td>
                          <td data-order="<?=strtotime($arrBanner['fecha_creacion']);?>"><?=date('d-m-Y',strtotime(str_replace('-','/', $arrBanner['fecha_creacion'])));?></td>
                          <td data-order="<?=strtotime($arrBanner['inicio_fecha']);?>"><?=date('d-m-Y',strtotime(str_replace('-','/', $arrBanner['inicio_fecha'])));?></td>
                          
						  <td>
                            
								<?php
									if($arrBanner['estado_banner'] == 1){
								?>
									<button type="button" class="btn btn-warning btn-xs">Pendiente</button>
								<?php
									} else if($arrBanner['estado_banner'] == 2){
								?>
									<button type="button" class="btn btn-success btn-xs">Aceptado</button>
								<?php
									} else if($arrBanner['estado_banner'] == 3){
								?>
									<button type="button" class="btn btn-info btn-xs">Finalizado</button>
								<?php
									} else if($arrBanner['estado_banner'] == 4){
								?>
									<button type="button" class="btn btn-danger btn-xs">Cancelado</button>
								<?php
									}
								?>
							
                          </td>
						  
                          <td>
                            <a href="g_banners_detail.php?idbanner=<?=$arrBanner['idbanner'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                            <a href="#" class="btn <?=($arrBanner['reporte']) == 0 ? 'btn-danger' : 'btn-success';?> btn-xs reporte<?=$arrBanner['idbanner'];?>" onclick="enviarReporte('<?=$arrBanner['idbanner'];?>','reporte<?=$arrBanner['idbanner'];?>','<?=($arrBanner['reporte']) == 0 ? '1' : '0';?>');"><i class="fa fa-paper-plane"></i> <?=($arrBanner['reporte']) == 0 ? 'Sin Reporte' : 'Con Reporte';?> </a>
                            
                          </td>
                        </tr>
						
						<?php
							}
						}	
						?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
	<!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- SweetAlert -->
    <script src="../js/sweetalert.min.js"></script>
	
	<!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
    <script>
		//$(function() {
			
			$('#datatable-responsive').DataTable({
				fixedColumns: {
					leftColumns: 2
				},
				
				scrollX:        true,
				
				fixedColumns: true,
				
				"order": [[ 2, "desc" ]]
				
			});
		//});
		
				
		//reporte enviado o no enviado
		function enviarReporte( idbanner, idclass, idreporte ) {
			//this.preventDefault();
			
			$('#load-form').fadeIn();
			
			$.ajax({
			  //url: "../objects/action.php",
			  type: "POST",
			  data: {opt:'enviarReporte',idBanner:idbanner,reporte:idreporte},
			  url: "../objects/actionBanner.php",
			  
			  success: function(data) {
				  
				  swal({
					title: 'Success',
					text: 'Reporte de banner modificado',
					icon: 'success',
					button: "Aww yiss!"
				  });
				  
				  $('#load-form').fadeOut();
				  
				  
					if(idreporte == 1){
						$('.reporte'+idbanner).html('<i class="fa fa-paper-plane"></i> Con reporte');
						$('.reporte'+idbanner).removeClass('btn-danger').addClass('btn-success');  
					} else {
						$('.reporte'+idbanner).html('<i class="fa fa-paper-plane"></i> Sin reporte');
						$('.reporte'+idbanner).removeClass('btn-success').addClass('btn-danger');  
					}
				  
				  
				},
				error: function(e) {
					swal({   
						title: 'Warning',
						text: 'Oops! Ha ocurrido un error.',
						icon: 'warning',
						button: "Ok"
					});
					
				}
			});
		};
	</script>
	
	
  </body>
</html>