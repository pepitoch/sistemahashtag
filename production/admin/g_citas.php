<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cCita.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oCita = new Cita($db);
	
	
	$vCita 		= $oCita->get_date_all();
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Citas <small>Lista de citas</small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Citas</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>&nbsp;</p>

                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>cod</th>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%">Ejecutivo Ventas</th>
                          <th>Nombre Cliente</th>
                          <th>Fecha Cita</th>
                          <th style="width: 10%">Estado</th>
                          <th style="width: 20%">#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php
					
						if($vCita){
							foreach ($vCita AS $id => $arrCita) {
						?>
                        <tr>
                          <td><?=$arrCita['fecha_cita'];?></td>
                          <td><?=$arrCita['codigo'];?></td>
                          <td>
                            <a><?=$arrCita['nombre'];?></a>
                            <br />
                            <small><?=$arrCita['correo'];?></small>
                          </td>
                          <td><?=$arrCita['nombre_cliente'];?></td>
                          <td><?=$dias[date('w', $arrCita['fecha_cita'])].' '.date('d', $arrCita['fecha_cita']).' de '.$meses[date('n', $arrCita['fecha_cita'])-1]. ' del '.date('Y', $arrCita['fecha_cita']) . ' / ' . $arrCita['hora_cita']?></td>
                         
						  <td>
                            
								<?php
									if($arrCita['estado_cita'] == 1){
								?>
									<button type="button" class="btn btn-warning btn-xs">Pendiente</button>
								<?php
									} else if($arrCita['estado_cita'] == 2){
								?>
									<button type="button" class="btn btn-success btn-xs">Aceptada</button>
								<?php
									} else if($arrCita['estado_cita'] == 3){
								?>
									<button type="button" class="btn btn-info btn-xs">Finalizada</button>
								<?php
									} else if($arrCita['estado_cita'] == 4){
								?>
									<button type="button" class="btn btn-danger btn-xs">Cancelada</button>
								<?php
									}
								?>
							
                          </td>	
                          <td>
                            <a href="g_citas_detail.php?idcita=<?=$arrCita['idcita'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                            <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                            <a onclick="deleteCita(<?=$arrCita['idcita'];?>);" href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                          </td>
                        </tr>
						
						<?php
							}
						}	
						?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
	<!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
	
	<!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    
	<!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
    <script>
		$(function() {
			
			$('#datatable-responsive').DataTable({
				"order": [[ 0, "desc" ]],
				"columnDefs": [
					{
						"targets": [ 0 ],
						"visible": false,
						"searchable": false
					}
				]
			});
		});
		
		function deleteCita(id_cita){
			$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'deleteCita',idCita:id_cita},
				  url: "../objects/action.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Cita Borrada',
						icon: 'success',
						button: "Aww yiss!"
					  }).then((value) => {
							location.reload();
						});
					  
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
		}
	</script>
  </body>
</html>