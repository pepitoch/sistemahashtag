<?php
	session_start();
	
	if(isset($_SESSION['iduser'])){
		header("Location: dashboard.php");
		exit();
	}

	
	
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Hashtag #</title>

    <?php include_once "c_css.php";?>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="POST" novalidate class="form-horizontal form-label-left">
              <h1># Admin Hashtag #</h1>
              <div class="item form-group ht-login-input">
                <input type="text" required class="form-control" name="email" placeholder="Username" required="" />
              </div>
              <div class="item form-group ht-login-input">
                <input type="password" required class="form-control" name="password" placeholder="Password" required="" />
              </div>
              <div>
				<input type="hidden" value="nLogin" name="opt" />
				<button id="send" type="submit" class="btn btn-default submit">LOGIN</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-hashtag"></i> Hashtag Agencia</h1>
                  <p>©<?=date('Y')?> All Rights Reserved. Hashtag Agencia.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        
      </div>
    </div>
	
	
	<!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- validator -->
    <script src="../../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="../../vendors/sweetalert/dist/sweetalert.min.js"></script>

	
	<!-- validator -->
    <script>
      // initialize the validator function
      var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})

      

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true, validatorResult = validator.checkAll(this);

        // evaluate the form using generic validaing
        if (!validatorResult.valid) {
			// console.log('holis');
			submit = false;
			
		}

        if (submit){
			
				$.ajax({
				  url: "../objects/actionLogin.php",
				  //url: "http://127.0.0.1/SistemaHashtag/production/objects/actionLogin.php",
				  type: "POST",
				  data:  new FormData(this),
				  contentType: false,
				  cache: false,
				  processData:false,
				  beforeSend : function(){
				  },
				  success: function(data) {
					var parsed = JSON.parse(data);
					if(parsed.text=="ok"){
                        window.location.href ='dashboard.php';
                        
                    }else{
                        swal({   
							title: 'Warning',
							text: 'Revisa la informacion proporcionada.',
							type: 'warning',
							confirmButtonText: "Ok"
						});
                    }
						$('form')[0].reset();
					},
					error: function(e) {
						swal({   
							title: 'Error',
							text: 'Revisa la informacion proporcionada.',
							type: 'error',
							confirmButtonText: "Ok"
						});
						$('form')[0].reset();
					}
				});
				return false;
				
			
		}
        

        return !!validatorResult.valid;
      });
    </script>
    <!-- /validator -->
    
  </body>
</html>
