<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cCita.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oCita = new Cita($db);
	
	if(isset($_GET['idcita'])){
		$vCita 		= $oCita->get_date_detail($_GET['idcita']);
		if($vCita){
			$ticket			= $vCita['codigo'];
			$nombre			= $vCita['nombre'];
			$correo			= $vCita['correo'];
			$cliente		= $vCita['nombre_cliente'];
			$fecha			= $vCita['fecha_cita'];
			$hora			= $vCita['hora_cita'];
			$historial		= $vCita['historial'];
			$comentario		= $vCita['comentario'];
			$creado			= $vCita['fecha_creacion'];
			$ip				= $vCita['ip'];
			$estado_cita	= $vCita['estado_cita'];
		}
	} else {
		header("Location: g_citas.php"); /* Redirect browser */
		exit();
	}
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
			<div class="ht-back">			
				<a onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i> Atr&aacute;s</a>
			</div>
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Citas Detalle <small></small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1><?=$cliente?></h1>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <ul class="stats-overview ht-stats-overview">
                        <li>
                          <span class="fa fa-calendar fa-4x"></span>
                          <h3 class=""> <?=$dias[date('w', $fecha)].' '.date('d', $fecha).' de '.$meses[date('n', $fecha)-1]. ' del '.date('Y', $fecha)?> </h3>
                          <span class="value text-success"> <?=date('d/m/Y', $fecha)?> </span>
                        </li>
                        <li>
                          <span class="fa fa-clock-o fa-4x"></span>
                          <h3 class=""> <?=$hora?> </h3>
                          <span class="value text-success"> &nbsp; </span>
                        </li>
                      </ul>
                      <br />

					<!-- start project-detail sidebar -->
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <section class="panel">

                        <div class="x_title">
                          <h2>Detalles</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                          <h3 class="green"><i class="fa fa-paint-brush"></i> No. Ticket: <?=$ticket?></h3>

                          <p>
							<?=$historial?>
						  </p>
                          <br />

                          <div class="project_detail">

                            <p class="title">Cliente / Empresa</p>
                            <p><?=$cliente?></p>
                            <p class="title">Ejecutivo de Ventas</p>
                            <p><?=$nombre?></p>
                            <p class="title">Correo Electronico</p>
                            <p><?=$correo?></p>
                            <p class="title">Comentarios Adicionales</p>
                            <p><?=$comentario?></p>
                          </div>

						  <hr/>
						  <div class="project_detail">

                            <p class="title">Solicitud Creada</p>
                            <p><?=$creado?></p>
                            <p class="title">IP</p>
                            <p><?=$ip?></p>
                          </div>
                          
                        </div>

                      </section>
					 </div>
                    <!-- end project-detail sidebar -->
					  
					<!-- start project-detail sidebar -->
                    <div class="col-md-4 col-sm-4 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>Progreso</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
							<p id="ht-text-cita">
								<?php
									if($estado_cita == 1){
										echo 'Cita pendiente por aceptar';
									} else if($estado_cita == 2){
										echo 'Cita aceptada segun la fecha';
									} else if($estado_cita == 3){
										echo 'Cita cerrada, ya se realizo la visita';
									} else if($estado_cita == 4){
										echo 'Cita cancelada';
									} else {
										echo 'No hay datos de progreso';
									}
								?>
							</p>
							<div class="project_progress">
								<?php
									if($estado_cita == 1){
										
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-orange ht-progress" role="progressbar" data-transitiongoal="25"></div>
								</div>
								<small class="ht-progress-text">25% Completo</small>
								<?php
									} else if($estado_cita == 2){
								?>
								<div class="progress progress_sm ">
									<div class="progress-bar bg-green ht-progress" role="progressbar" data-transitiongoal="75"></div>
								</div>
								<small class="ht-progress-text">75% Completo</small>
								<?php
									} else if($estado_cita == 3){
								?>
								<div class="progress progress_sm ">
									<div class="progress-bar bg-blue-sky ht-progress" role="progressbar" data-transitiongoal="100"></div>
								</div>
								<small class="ht-progress-text">100% Completo</small>
								<?php
									} else if($estado_cita == 4){
								?>
								<div class="progress progress_sm">
									<div class="progress-bar bg-red ht-progress" role="progressbar" data-transitiongoal="100"></div>
								</div>
								<small class="ht-progress-text">100% Completo</small>
								<?php
									}
								?>
							</div>
                          <br />

                          <div class="text-left mtop20">
							<?php
								if($estado_cita == 1){
									echo '<a href="#" class="btn btn-sm btn-success btn-block" id="ht-aceptar">Aceptar</a><br/>
											<a href="#" class="btn btn-sm btn-info btn-block" id="ht-finalizar">Finalizar</a><br/>
											<a href="#" class="btn btn-sm btn-danger btn-block" id="ht-cancelar">Cancelar</a>';
								} else if($estado_cita == 2){
									echo '<a href="#" class="btn btn-sm btn-info btn-block" id="ht-finalizar">Finalizar</a>';
								} else {
									echo '';
								}
							?>
                            
                          </div>
                        </div>

                      </section>

                    </div>
                    <!-- end project-detail sidebar -->


                    </div>

                    

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	<script>
		$(document).ready(function() {
			var id_cita = <?=$_GET['idcita'];?>;
			
			//aceptar
			$( "#ht-aceptar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'doneCita',idCita:id_cita},
				  url: "../objects/action.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Cita aceptada',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-cita').text('Cita aceptada segun la fecha');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'75',
						  'style':'width: 75%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-green');
					  $('.ht-progress-text').text('75% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			//finalizar
			$( "#ht-finalizar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'finCita',idCita:id_cita},
				  url: "../objects/action.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Cita finalizada',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-cita').text('Cita cerrada, ya se realizo la visita');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass();
					  $('.progress div').addClass('progress-bar bg-blue-sky');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			
			//cancelar
			$( "#ht-cancelar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'cancelCita',idCita:id_cita},
				  url: "../objects/action.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'Cita cancelada',
						icon: 'success',
						button: "Ok"
					  });
					  $('#ht-text-cita').text('Cita cancelada');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-red');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
		});
	</script>
	

  </body>
</html>