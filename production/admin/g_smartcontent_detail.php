<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cSmartContent.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oSmartContent = new SmartContent($db);
	
	if(isset($_GET['idsmart'])){
		$vSmartContent 		= $oSmartContent->get_smart_detail($_GET['idsmart']);
		if($vSmartContent){
			$ticket			= $vSmartContent['codigo'];
			$cod_vendedor	= $vSmartContent['codigo_vendedor'];
			$nombre			= $vSmartContent['nombreV'];
			$correo			= $vSmartContent['correoV'];
			$cliente		= $vSmartContent['nombre_empresa'];
			$difusiones		= $vSmartContent['difusiones'];
			$red			= $vSmartContent['red_social'];
			$inversion		= $vSmartContent['inversion'];
			$fecha_v		= $vSmartContent['fecha_visita'];
			$fecha_r		= $vSmartContent['fecha_reportaje'];
			$fecha_p		= $vSmartContent['fecha_publicacion'];
			$comentario		= $vSmartContent['mensaje'];
			$creado			= $vSmartContent['fecha_ingreso'];
			$creado_last	= $vSmartContent['fecha_modificacion'];			
			$estado			= $vSmartContent['estado'];
		}
	} else {
		header("Location: g_smartcontent.php"); /* Redirect browser */
		exit();
	}
	
	function number_to_money($value, $symbol = '$', $decimals = 2)
	{
		return $symbol . ($value < 0 ? '-' : '') . number_format(abs($value), $decimals);
	}
	
	setlocale(LC_ALL,"es_ES");
	date_default_timezone_set('America/el_salvador');
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
		<?php include_once "menu.php";?>
		
        <!-- page content -->
        <div class="right_col" role="main">
			<div class="ht-back">			
				<a onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i> Atr&aacute;s</a>
			</div>
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>SmartContents Detalle <small></small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1><?=$cliente?></h1>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <ul class="stats-overview ht-stats-overview-3">
                        <li>
                          <span class="fa fa-globe fa-4x"></span>
                          <h3 class=""> <?=$difusiones;?> </h3>
                          <span class="value text-success"> Difusiones </span>
                        </li>
                        <li>
                          <span class="fa fa-usd fa-4x"></span>
                          <h3 class=""> <?=number_to_money($inversion);?> </h3>
                          <span class="value text-success"> +IVA </span>
                        </li>
                        <li class="last-li">
                          <span class="fa fa-calendar fa-4x"></span>
                          <h3 class=""> <?=date('d-m-Y',strtotime(str_replace('-','/', $fecha_p)));?> </h3>
                          <span class="value text-success"> Fecha Aprox de Publicacion </span>
                        </li>
                      </ul>
                      <br />

					<!-- start project-detail sidebar -->
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <section class="panel">

                        <div class="x_title">
                          <h2>Detalles</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                          <h3 class="green"><i class="fa fa-paint-brush"></i> No. Ticket: <?=$ticket?></h3>
							<p><?=$comentario?></p>
                          <br />
						  
							<div class="col-md-12 col-sm-12 col-xs-12">

							  <div class="project_detail">

								<p class="title">Cliente / Empresa</p>
								<p><?=$cliente?></p>
								<p class="title">No. de Difusiones</p>
								<p><?=$difusiones?></p>
								<p class="title">Plataformas de Publicacion</p>
								<p class="check-social-1">
									<?php
										$tiposSmartContentX = explode(",", $red);
										$tX = '';
										
										for($i=0; $i<count($tiposSmartContentX); $i++){
											
											if($tiposSmartContentX[$i] != ''){
												
												if($tiposSmartContentX[$i] == 1){
													$tX .= ' <span class="fa fa-facebook fa-2x"></span> Facebook /';
												} else if($tiposSmartContentX[$i] == 2){
													$tX .= ' <span class="fa fa-twitter fa-2x"></span> Twitter /';
												} else if($tiposSmartContentX[$i] == 3){
													$tX .= ' <span class="fa fa-instagram fa-2x"></span> Instagram /';
												}
											}
										}
										
										echo substr($tX, 0, -1);
									
									?>
								</p>
								<hr/>
								<p class="title">Codigo Ejecutivo de Ventas</p>
								<p><?=$cod_vendedor?></p>
								<p class="title">Ejecutivo de Ventas</p>
								<p><?=$nombre?></p>
								<p class="title">Correo Electronico</p>
								<p><?=$correo?></p>
							  </div>
							  
							
							
							

							  <hr/>
							  <div class="project_detail">

								<p class="title">Solicitud Creada</p>
								<p><?=$creado?></p>
								<p class="title">&Uacute;ltima actualizaci&oacute;n</p>
								<p><?=$creado_last?></p>
							  </div>
							
							</div>
							
							
							
                        </div>

                      </section>
					 </div>
                    <!-- end project-detail sidebar -->
					  
					<!-- start project-detail sidebar -->
                    <div class="col-md-4 col-sm-12 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>Fechas</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
							<div class="project_detail">

								<p class="title">Fecha Visita</p>
								<p><?=$dias[date('w', strtotime($fecha_v))]." ".date('d', strtotime($fecha_v))." de ".$meses[date('n', strtotime($fecha_v))-1]. " del ".date('Y', strtotime($fecha_v));?></p>
								<p class="title">Fecha Reportaje</p>
								<p><?=$dias[date('w', strtotime($fecha_r))]." ".date('d', strtotime($fecha_r))." de ".$meses[date('n', strtotime($fecha_r))-1]. " del ".date('Y', strtotime($fecha_r));?></p>
								<p class="title">Fecha Publicaci&oacute;n</p>
								<p><?=$dias[date('w', strtotime($fecha_p))]." ".date('d', strtotime($fecha_p))." de ".$meses[date('n', strtotime($fecha_p))-1]. " del ".date('Y', strtotime($fecha_p));?></p>
							</div>
							
                          <br />

                        </div>

                      </section>

                    </div>
                    <!-- end project-detail sidebar -->


                    </div>

                    

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
	<script>
		//$(document).ready(function() {
			var id_smart = <?=$_GET['idsmart'];?>;
			
			//aceptar
			$( "#ht-aceptar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'doneSmartContent',idSmartContent:id_smart},
				  url: "../objects/actionSmartContent.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'SmartContent aceptado',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-smart').text('SmartContent aceptado segun la fecha');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'75',
						  'style':'width: 75%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-green');
					  $('.ht-progress-text').text('75% Completo');
					  $('#ht-aceptar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			//finalizar
			$( "#ht-finalizar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'finSmartContent',idSmartContent:id_smart},
				  url: "../objects/actionSmartContent.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'SmartContent finalizada',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-smart').text('SmartContent cerrada, ya se realizo!');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass();
					  $('.progress div').addClass('progress-bar bg-blue-sky');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
			
			
			//cancelar
			$( "#ht-cancelar" ).click(function( event ) {
				event.preventDefault();
				
				$.ajax({
				  //url: "../objects/action.php",
				  type: "POST",
				  data: {opt:'cancelSmartContent',idSmartContent:id_smart},
				  url: "../objects/actionSmartContent.php",
				  
				  success: function(data) {
					  
					  swal({
						title: 'Success',
						text: 'SmartContent cancelado',
						icon: 'success',
						button: "Aww yiss!"
					  });
					  $('#ht-text-smart').text('SmartContent cancelado');
					  $('.ht-progress').attr({
						  'data-transitiongoal':'100',
						  'style':'width: 100%;'
					  });
					  $('.ht-progress').removeClass('bg-orange');
					  $('.ht-progress').addClass('bg-red');
					  $('.ht-progress-text').text('100% Completo');
					  $('#ht-aceptar').hide();
					  $('#ht-finalizar').hide();
					  $('#ht-cancelar').hide();
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Oops! Ha ocurrido un error.',
							icon: 'warning',
							button: "Ok"
						});
						
					}
				});
			});
			
		// });
	</script>

  </body>
</html>