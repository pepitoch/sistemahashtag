<?php
//session_start();

?>

<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
	  <a href="index.php" class="site_title"><i class="fa fa-hashtag"></i> <span>Gestiones HT</span></a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -->
	<div class="profile">
	  <div class="profile_pic">
		<img src="../images/hashtag.png" alt="..." class="img-circle profile_img">
	  </div>
	  <div class="profile_info">
		<span>Bienvenido,</span>
		<h2><?=$_SESSION['name']?></h2>
	  </div>
	</div>
	<!-- /menu profile quick info -->

	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	  <div class="menu_section">
		<h3>-</h3>
		<ul class="nav side-menu">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-home"></i> Inicio
				</a>
			</li>
			<li>
				<a href="g_citas.php">
					<i class="fa fa-clock-o"></i> Gestionar Citas
				</a>
			</li>
			<li>
				<a href="g_banners.php">
					<i class="fa fa-picture-o"></i> Gestionar BANNER
				</a>
			</li>
			<li class="bolsa-trabajo">
				<a href="g_anuncio.php">
					<i class="fa fa-briefcase"></i> Bolsa de Trabajo
				</a>
			</li>
			<li class="bolsa-trabajo-1">
				<a href="g_anuncio_online.php">
					<i class="fa fa-globe"></i> Bolsa de Trabajo Online
				</a>
			</li>
			<li>
				<a href="g_emailing.php">
					<i class="fa fa-envelope"></i> Emailing
				</a>
			</li>
			<li>
				<a href="g_smartcontent.php">
					<i class="fa fa-newspaper-o"></i> SmartContent
				</a>
			</li>
		  
		</ul>
	  </div>

	</div>
	<!-- /sidebar menu -->

	
  </div>
</div>


 <!-- top navigation -->
        <div class="top_nav navbar-fixed-top">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="../images/hashtag.png" alt=""><?=$_SESSION['name']?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="../objects/logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
