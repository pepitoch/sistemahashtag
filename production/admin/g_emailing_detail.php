<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cEmailing.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oEmailing = new Emailing($db);
	
	if(isset($_GET['idemailing'])){
		$vEmailing 		= $oEmailing->get_emailing_detail($_GET['idemailing']);
		if($vEmailing){
			$ticket			= $vEmailing['codigo'];
			$cod_vendedor	= $vEmailing['codigo_vendedor'];
			$nombre			= $vEmailing['nombreV'];
			$correo			= $vEmailing['correoV'];
			$cliente		= $vEmailing['nombre_empresa'];
			$asunto			= $vEmailing['asunto'];
			$correo_e		= $vEmailing['correo_remitente'];
			$url			= $vEmailing['url'];
			$files			= $vEmailing['imagen'];
			$n_envios		= $vEmailing['envios'];
			$inversion		= $vEmailing['inversion'];
			$fecha			= $vEmailing['fecha'];
			$comentario		= $vEmailing['mensaje'];
			$creado			= $vEmailing['fecha_ingreso'];
			$creado_update	= $vEmailing['fecha_modificacion'];
			$estado			= $vEmailing['estado'];
		}
		
		// fechas registradas para correos
		$vEmailingDate 		= $oEmailing->get_emailing_fecha($_GET['idemailing']);
		
	} else {
		header("Location: g_emailing.php"); /* Redirect browser */
		exit();
	}
	
	function number_to_money($value, $symbol = '$', $decimals = 2)
	{
		return $symbol . ($value < 0 ? '-' : '') . number_format(abs($value), $decimals);
	}
	
	setlocale(LC_ALL,"es_ES");
	date_default_timezone_set('America/el_salvador');
	// // setlocale(LC_MONETARY, 'en_US');
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
			<div class="ht-back">			
				<a onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i> Atr&aacute;s</a>
			</div>
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banners Detalle <small></small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1><?=$cliente?></h1>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <ul class="stats-overview ht-stats-overview-3">
                        <li>
                          <span class="fa fa-envelope fa-4x"></span>
                          <h3 class=""> <?=$n_envios;?> </h3>
                          <span class="value text-success"> Env&iacute;os </span>
                        </li>
                        <li>
                          <span class="fa fa-usd fa-4x"></span>
                          <h3 class=""> <?=number_to_money($inversion);?> </h3>
                          <span class="value text-success"> +IVA </span>
                        </li>
                        <li class="last-li">
                          <span class="fa fa-calendar fa-4x"></span>
                          <h3 class=""> <?=date('d-m-Y',strtotime(str_replace('-','/', $fecha)));?> </h3>
                          <span class="value text-success"> Fecha Aprox. </span>
                        </li>
                      </ul>
                      <br />
						
						<div class="col-xs-12">
						
							<?php
								$value_date = round(($oEmailing->get_count_emailing_envio($_GET['idemailing']) * 100) / $n_envios);
												
							?>
							
							<p id="ht-text-banner">
								<?=($value_date != 100)?'Pendiente de programar el resto de envios de campa&ntilde;a por correo.':'Programaci&oacute;n de env&iacute;os de correo completa.'?>
							</p>
							
							<div class="project_progress">
								
								<div class="progress progress_sm">
									<div class="progress-bar <?=($value_date != 100)?'bg-orange':'bg-green'?> ht-progress" role="progressbar" data-transitiongoal="<?=$value_date?>"></div>
								</div>
								<small class="ht-progress-text"><?=$value_date?>% Completo</small>
								
							</div>
						</div>
						<p>&nbsp;</p>	
						<?php
							if($vEmailingDate){
								
						?>
						<div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>No. Env&iacute;o</th>
                                <th>Fecha de Campa&ntilde;a</th>
                                
                              </tr>
                            </thead>
                            <tbody>
								<?php
									
									foreach ($vEmailingDate AS $id => $arrEmailDate) {
										
								?>
									<tr>
										<td>Env&iacute;o <?=$arrEmailDate['correlativo'];?></td>
										<td><?=$dias[date('w', strtotime($arrEmailDate['fecha']))]." ".date('d', strtotime($arrEmailDate['fecha']))." de ".$meses[date('n', strtotime($arrEmailDate['fecha']))-1]. " del ".date('Y', strtotime($arrEmailDate['fecha'])) . " a las " . date('h:i:s A', strtotime($arrEmailDate['fecha']));?></td>
										
									</tr>
								<?php
									}
								?>
                              
                            </tbody>
                          </table>
                        </div>
						<?php
							}
						?>

					<!-- start project-detail sidebar -->
                    <div class="col-md-7 col-sm-12 col-xs-12">
                      <section class="panel">

                        <div class="x_title">
                          <h2>Detalles</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                          <h3 class="green"><i class="fa fa-paint-brush"></i> No. Ticket: <?=$ticket?></h3>
							<p><?=$comentario?></p>
                          <br />
						  
							

							  <div class="project_detail">

								<p class="title">Cliente / Empresa</p>
								<p><?=$cliente?></p>
								<p class="title">Asunto de correo</p>
								<p><?=$asunto?></p>
								<p class="title">Correo remitente</p>
								<p><?=$correo_e?></p>
								<p class="title">URL para Emailing</p>
								<p><?=$url?></p>
								<p class="title">N&uacute;mero de Env&iacute;os</p>
								<p><?=$n_envios?></p>
								<p class="title">Inversi&oacute;n</p>
								<p><?=$inversion?></p>
								<p class="title">Fecha estimada de publicaci&oacute;n</p>
								<p><?=$fecha?></p>
								<p class="title">Observaciones / Mensaje</p>
								<p><?=$comentario?></p>
								
								
								<hr/>
								<h4>Arte / Imagen</h4>
								<div class="product-image">
									<img src="<?=URL_HT.substr($files, 3);?>" alt="<?=$cliente?>" class="img-responsive img-thumbnail">
								</div>
								
								<hr/>
								<p class="title">Codigo Ejecutivo de Ventas</p>
								<p><?=$cod_vendedor?></p>
								<p class="title">Ejecutivo de Ventas</p>
								<p><?=$nombre?></p>
								<p class="title">Correo Electronico</p>
								<p><?=$correo?></p>
								
							  </div>
							  
							
							
							

							  <hr/>
							  <div class="project_detail">

								<p class="title">Solicitud Creada</p>
								<p><?=$creado?></p>
								<p class="title">&Uacute;ltima actualizaci&oacute;n</p>
								<p><?=$creado_update?></p>
							  </div>
							
							
								
								
							
							
                        </div>

                      </section>
					 </div>
                    <!-- end project-detail sidebar -->
					  
					<!-- start project-detail sidebar -->
                    <div class="col-md-5 col-sm-12 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>Programar Fechas</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
							
							<div class="project_detail">
								
								<?php
									for($i=1;$i<=$n_envios;$i++){
								?>
								<p class="title">Env&iacute;o No. <?=$i?></p>
								<p>
									<div class="item form-group ht-date">
										
										<div class="col-md-8 col-sm-8 col-xs-8 has-feedback-left">
											<input value="<?=($oEmailing->get_fecha_asignada($_GET['idemailing'],$i))? date('m/d/Y g:i A',strtotime($oEmailing->get_fecha_asignada($_GET['idemailing'],$i))):date('m/d/Y g:i A')?>" readonly="readonly" required="required" type="text" class="form-control has-feedback-left single_cal" id="single_cal<?=$i?>" name="single_cal3" placeholder="fecha" aria-describedby="dateBanner">
											<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
											<span id="inputSuccess2Status3" class="sr-only">(success)</span>
											
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4 has-feedback-left">
											<a href="#" class="btn btn-sm btn-primary" onclick="getVal('single_cal<?=$i?>','<?=$i?>')">Programar</a>
										</div>
									</div>
								</p>
								</br></br></br>
								<div class="clearfix"></div>
								</hr>
								<?php
									}
								?>
								
							</div>
							
                        </div>

                      </section>

                    </div>
                    <!-- end project-detail sidebar -->


                    </div>

                    

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- bootstrap-daterangepicker -->
    <script src="../../vendors/moment/min/moment.min.js"></script>
    <script src="../../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
	<!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
    <!-- picker -->
	<?php
		$datetime = new DateTime('tomorrow');
		
		$fecha = date('m/d/Y');
		$nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'm/d/Y' , $nuevafecha );
		
	?>
	<script>
		//obtener valor de fechas programadas para cada emailing
		function getVal(id_class,ai) {
			var x = document.getElementById(id_class);
			$(x).val();
			//alert('holis '+$(x).val());
			$('#load-form').fadeIn();
			
			$.ajax({
			  type: "POST",
			  data: {opt:'addDates',idEmailing:<?=$_GET['idemailing']?>,fecha:$(x).val(),correlativo:ai},
			  url: "../objects/actionEmailing.php",
			  
			  success: function(data) {
					var parsed = JSON.parse(data);
					swal({
						title: parsed.title,
						text: parsed.text,
						icon: parsed.type,
						button: "Aww yiss!"
					}).then((value) => {
						location.reload();
					});
					
				  $('#load-form').fadeOut();
				},
				error: function(e) {
					swal({   
						title: 'Warning',
						text: 'Oops! Ha ocurrido un error.',
						icon: 'warning',
						button: "Ok"
					});
					$('#load-form').fadeOut();
				}
			});
		}
		
		
      $(document).ready(function() {
				  
        $('.single_cal').daterangepicker({
			 autoUpdateInput: true,
			 timePicker: true,
			 timePickerIncrement: 30,
			 "locale": {
				"format": "MM/DD/YYYY h:mm A",
				"separator": " - ",
				"applyLabel": "Aplicar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "Hasta",
				"customRangeLabel": "Modificar",
				"weekLabel": "S",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				]
			},
          singleDatePicker: true,
          calender_style: "picker_3",
		  //startDate: "<?php echo date('m-d-Y');?>",
		  //minDate: "<?php echo $datetime->format('m-d-Y');?>",
		  maxDate: "<?php echo $nuevafecha;?>"
        }, function(start, end, label) {
          
        });
		
		$('#single_cal3').on('apply.daterangepicker', function(ev, picker) {
			//$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			$('.ht-date').removeClass('bad');
			$('.ht-date .alert').hide();
		});

		$('#single_cal3').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
		
        
      });
    </script>
	

  </body>
</html>