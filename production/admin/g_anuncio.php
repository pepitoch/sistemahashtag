<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cAnuncio.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oAnuncio = new Anuncio($db);
	
	
	$vAnuncio 		= $oAnuncio->get_anuncio_all();
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banners <small>Lista de banners</small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			
			<div class="row tile_count">
			
			
				<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<a href="g_anuncio_new.php">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-plus-circle"></i></div>
								<div class="count">Nuevo<small> anuncio</small></div>
								<h3>Crear nuevo anuncio</h3>
								<p>
									Aqui podras crear un nuevo anuncio sin un ticke creado por el ejecutivo de ventas.
								</p>
							</div>
						</a>
					</div>
					<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<a href="g_anuncio_online.php">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-pencil-square"></i></div>
								<div class="count">Anuncios <small> Bolsa de Trabajo</small></div>
								<h3>Anuncios activos e inactivos</h3>
								<p>
									Aqui podras modificar los anuncios activo o inactivos de Bolsa de Trabajo
								</p>
							</div>
						</a>
					</div>
					
				
				</div>
				
			  </div>
			  <!-- /top tiles -->
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Banners para Bolsa de Trabajo</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>&nbsp;</p>

                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-hover table-bordered dt-responsive display nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          
							<th >Ejecutivo Ventas</th>
							<th style="width: 20%;">Nombre Cliente</th>
							<th style="width: 20%;">Correo Cliente</th>
							<th style="width: 20%;">Fecha Creacion</th>
							<th>Tipo de Anuncio</th>
							<th >#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php
					
						if($vAnuncio){
							foreach ($vAnuncio AS $id => $arrAnuncio) {
						?>
                        <tr >
                          
                          
                          <td <?=($arrAnuncio['estado']==1)?'style="border-left: 8px solid gray;"':'';?>>
                            <a><?=$arrAnuncio['nombreV'];?></a>
                            <br />
                            <small><?=$arrAnuncio['correoV'];?></small>
                          </td>
                          <td>
                            <a><?=$arrAnuncio['nombre'];?></a>
                            
                          </td>
                          <td>
                            <?=$arrAnuncio['repuesta_correo'];?>
                          </td>
                          <td data-order="<?=strtotime($arrAnuncio['fecha_ingreso']);?>">
                            <?=$arrAnuncio['fecha_ingreso'];?>
                          </td>
						  
							<?php
								if($arrAnuncio['idtipo_anuncio'] == 1){
									
									echo "<td class='info'>Explorador de Empleos</td>";
									
								} else {
									
									echo "<td class='success'>Empresa Destacada</td>";
								}
							?>
						  
                          
							<td>
								<a href="g_anuncio_detail.php?idanuncio=<?=$arrAnuncio['idanuncio'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> VER </a>
                            
							</td>
                        </tr>
						
						<?php
							}
						}	
						?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
	<!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
	
	<!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
    <script>
		//$(function() {
			
			$('#datatable-responsive').DataTable({
				"order": [[ 3, "desc" ]]
				
			});
		//});
	</script>
  </body>
</html>