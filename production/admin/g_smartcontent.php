<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cSmartContent.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oSmartContent = new SmartContent($db);
	
	
	$vSmartContent 		= $oSmartContent->get_smart_all();
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>SmartContent <small>Lista de Campa&ntilde;as SmartContent</small></h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Campa&ntilde;as SmartContent</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>&nbsp;</p>

                    <!-- start project list -->
                    <table id="datatable-responsive" class="table table-striped table-hover table-bordered dt-responsive display nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          
                          
                          <th >Ejecutivo Ventas</th>
                          <th>Nombre Cliente</th>
                          <th>Fecha CREACION TICKET</th>
                          <th >No. Env&iacute;os</th>
                          <th >Fecha Aprox. Visita</th>
                          <th >Fecha Aprox. Reportaje</th>						  
                          <th >#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php
					
						if($vSmartContent){
							foreach ($vSmartContent AS $id => $arrSmartContent) {
						?>
                        <tr>
                          
                          
                          <td>
                            <a><?=$arrSmartContent['nombreV'];?></a>
                            <br />
                            <small><?=$arrSmartContent['correoV'];?></small>
                          </td>
                          <td><?=$arrSmartContent['nombre_empresa'];?></td>
                          <td data-order="<?=strtotime($arrSmartContent['fecha_ingreso']);?>"><?=date('d-m-Y',strtotime(str_replace('-','/', $arrSmartContent['fecha_ingreso'])));?></td>
                          <td><?=$arrSmartContent['difusiones'];?></td>
                          <td data-order="<?=strtotime($arrSmartContent['fecha_visita']);?>"><?=date('d-m-Y',strtotime(str_replace('-','/', $arrSmartContent['fecha_visita'])));?></td>
                          <td data-order="<?=strtotime($arrSmartContent['fecha_reportaje']);?>"><?=date('d-m-Y',strtotime(str_replace('-','/', $arrSmartContent['fecha_reportaje'])));?></td>
                         						  
                          <td>
                            <a href="g_smartcontent_detail.php?idsmart=<?=$arrSmartContent['idsmartcontent'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                            
                          </td>
                        </tr>
						
						<?php
							}
						}	
						?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
	<!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
	
	<!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
    <script>
		//$(function() {
			
			$('#datatable-responsive').DataTable({
				fixedColumns: {
					leftColumns: 2
				},
				
				scrollX:        true,
				
				fixedColumns: true,
				
				"order": [[ 2, "desc" ]]
				
			});
		//});
	</script>
  </body>
</html>