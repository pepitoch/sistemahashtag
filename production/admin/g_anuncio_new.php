<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cAnuncio.php";
	
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oAnuncio = new Anuncio($db);
	
	
	function number_to_money($value, $symbol = '$', $decimals = 2)
	{
		return $symbol . ($value < 0 ? '-' : '') . number_format(abs($value), $decimals);
	}
	
	$vAnuncioPremium	= $oAnuncio->get_count_anunciop();
	$vAnuncioBasic		= $oAnuncio->get_count_anunciob();
	$vAnuncioAll		= $oAnuncio->get_count_anuncio();
	
	
	// set your default timezone
	date_default_timezone_set('America/Tegucigalpa');
	setlocale(LC_ALL,"es_SV");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Anuncio Detalle </h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            
			<form class="form-horizontal form-label-left" method="POST" novalidate enctype="multipart/form-data">
				
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Cliente</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							
								<div class="item form-group">
									<div class="col-md-6">
										<label>Estado de anuncio para publicar:</label>
										<div class="sidebar-widget">
											<select class="form-control" name="status">
												<option value="0">Agregar anuncio pero sin publicar</option>
												<option value="1">Agregar anuncio y publicar</option>
											</select>
										</div>
									</div>
									
									
								</div>
							<br />
							
								<div class="item form-group">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<label>Tipo de anuncio a publicar *:</label>
										<div class="sidebar-widget">
											<div class="radio">
											  <label>
												<input onclick="show1();" type="radio" name="tipoanuncio" id="tipoanuncio1" value="1" required="required" checked>
												
												Anuncio b&aacute;sico para <b>Explorador de Empleos</b>
												
											  </label>
											</div>
										
											<div class="radio">
											  <label>
												<input onclick="show2();" type="radio" name="tipoanuncio" id="tipoanuncio2" value="2" required="required">
												Anuncio premium para <b>Empresas Destacadas</b>
											  </label>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="divider-dashed"></div>	
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
									
										<label>Nombre de la empresa: *</label>
										<input type="text" class="form-control" name="nEmpresa" placeholder="Nombre de empresa a publicar" required="required" >
										<p class="help-block">Si alguna empresa quiere el anonimato colocar un nombre que pueda identificar el anuncio por ejemplo: nombre del ejecutivo de ventas o de alguna persona responsable del anuncio.</p>
									</div>
								</div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="form-group">
										<label>Descripcion de la empresa:</label>
										<input type="text" class="form-control" name="nDescripcion" placeholder="Descripcion de empresa a publicar" required="required" >
										<p class="help-block">Puede ser el nombre completo de la empresa o el mismo texto de nombre. Este campo no es obligatorio.</p>
									</div>
								</div>
								
								<div class="clearfix"></div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
									
										<label>Correo electronico de la empresa: *</label>
										<input type="email" class="form-control" name="nEmail" placeholder="Correo electronico para Hoja de Vida" required="required" >
										<p class="help-block">Correo electronico donde se enviaran las Hojas de Vida.</p>
									</div>
								</div>
								
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
										<label>Imagen / PDF del anuncio: </label>
										<input type="file" class="form-control" name="filelife" id="imagenanuncio" required="required">
										<p class="help-block">Imagen o PDF del anuncio a publicar.</p>
									</div>
								</div>
								
						  </div>
						  
							<div id="anuncio-premium">
								<div class="x_title">
									<br/>
									<h2><br/><br/>Informaci&oacute;n de Especial para Empresas Destacadas</h2>
									
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									
									<p>&nbsp;</p>
									<div class="form-group text-center">
										<div class="item col-md-4 col-sm-4 col-xs-12">
											<label>Imagen / FONDO PRINCIPAL: </label>
											<input type="file" class="form-control" name="imgFondo" id="imgFondo" required="required">
											<p class="help-block">Imagen de fondo en el formulario.</p>
										</div>
										<div class="item col-md-4 col-sm-4 col-xs-12">
											<label>Imagen / FONDO DE FRASE: </label>
											<input type="file" class="form-control" name="imgFondo2" id="imgFondo2" required="required">
											<p class="help-block">Imagen de fondo en la descripcion o frase de la empresa.</p>
										</div>
										<div class="item col-md-4 col-sm-4 col-xs-12">
											<label>Imagen / LOGO DE CLIENTE: </label>
											<input type="file" class="form-control" name="imgLogoM" id="imgLogoM" required="required">
											<p class="help-block">Imagen de logo del cliente.</p>
										</div>
										
									</div>
									
									<div class="divider-dashed"></div>	
									<p>&nbsp;</p>
									
									<div class="item form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<label>Frase o Historia de la empresa: </label>
											<textarea name="nFrase" class="form-control" rows="4" required="required"></textarea>
										</div>
										
									</div>
									<p>&nbsp;</p>
									
									<div class="item form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<label>Direccion de la empresa: </label>
											<textarea name="nDireccion" class="form-control" rows="4" ></textarea>
										</div>
										
									</div>
									
									<div class="divider-dashed"></div>	
									<p>&nbsp;</p>
									
									<div class="form-group text-center">
										<div class="item col-md-6 col-sm-6 col-xs-12">
											<label><span class="fa fa-internet-explorer fa-3x"></span></label>
											<input type="text" class="form-control" name="nWeb" placeholder="URL de sitio web" required="required">
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label><span class="fa fa-phone fa-3x"></span></label>
											<input type="text" class="form-control" name="nPhone" placeholder="Numero de telefono" value="">
										</div>
										
									</div>
									<p>&nbsp;</p>
									<div class="form-group text-center">
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-facebook fa-3x"></span></label>
											<input type="text" class="form-control" name="nRedSocial[]" placeholder="URL de facebook" >
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-twitter fa-3x"></span></label>
											<input type="text" class="form-control" name="nRedSocial[]" placeholder="URL de twitter" >
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-youtube-play fa-3x"></span></label>
											<input type="text" class="form-control" name="nRedSocial[]" placeholder="URL de youtube" >
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<label><span class="fa fa-instagram fa-3x"></span></label>
											<input type="text" class="form-control" name="nRedSocial[]" placeholder="URL de instagram">
										</div>
										
									</div>
									
									
									
									
								</div>
								<div class="clearfix"></div>
								
								<div class="x_title">
									<br/>
									<h3><br/><br/>Plazas que ofrece</h3>
									
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									
									<p>&nbsp;</p>
									
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="item form-group">
										
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 1" value="">
											
										</div>
										<div class="item form-group">
										
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 2" value="">
											
										</div>
										<div class="item form-group">
										
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 3" value="">
											
										</div>
										<div class="item form-group">
										
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 4" value="">
											
										</div>
										<div class="item form-group">
										
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 5" value="">
											
										</div>
									</div>
									
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="form-group">
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 6" value="">
											
										</div>
										<div class="form-group">
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 7" value="">
											
										</div>
										<div class="form-group">
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 8" value="">
											
										</div>
										<div class="form-group">
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 9" value="">
											
										</div>
										<div class="form-group">
											<label>Plaza:</label>
											<input type="text" class="form-control" name="plaza[]" placeholder="Plaza 10" value="">
											
										</div>
									</div>
									
									
									
								</div>
							</div>
							
							<div class="clearfix"></div>
							<br/>
							<div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" value="wsAnuncio" name="opt" />
									<button id="send" type="submit" class="btn btn-success btn-block btn-lg">Enviar Publicacion de Bolsa de Trabajo</button>
								</div>

							</div>
					  </div>
					</div>
				</div>
			</form>
			
			
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
   
    <!-- validator -->
    <script src="../../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
	
    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
	
	
	<script type="text/javascript">
		
		function show1(){
		  document.getElementById('anuncio-premium').style.display ='none';
		}
		function show2(){
		  document.getElementById('anuncio-premium').style.display = 'block';
		}

	</script>
	
	<script>
		
		$(document).ready(function() {
		
			if($('#tipoanuncio1').is(':checked')) { 
				document.getElementById('anuncio-premium').style.display ='none';
			} else {
				document.getElementById('anuncio-premium').style.display = 'block';
			}
        
		});
    </script>
	
	
	<!-- validator -->
    <script>
      // initialize the validator function
     // validator.message.inversion = 'not a real date';

		var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})
		

		$('form').submit(function(e){
			
			
			
			var submit = true, validatorResult = validator.checkAll(this);
			
			if (!validatorResult.valid) {
				console.log('holis');
				submit = false;
				
			}
			
			if (submit){
				$('#load-form').fadeIn();
				console.log('submit');
				// var data = JSON.stringify( $(this).serializeArray() ); //  <-----------

				// console.log( data );
				
				//$(document).on('submit', '#create-business-form', function() {
					$.ajax({
					  //url: "../objects/actionAnuncio.php",
					  url: "../objects/actionAnuncio.php",
					  type: "POST",
					  data:  new FormData(this),
					  contentType: false,
					  cache: false,
					  processData:false,
					  beforeSend : function(){
						
					  },
					  success: function(data) {
						var parsed = JSON.parse(data);
						swal({
							title: parsed.title,
							text: parsed.text,
							icon: parsed.type,
							button: "Aww yiss!"
						}).then((value) => {
							window.location.replace("g_anuncio.php");
						});
						// $('form')[0].reset();
						// $('#load-form').fadeOut();
							
						},
						error: function(e) { 
							swal({   
								title: 'Warning',
								text: 'Revisa la informacion proporcionada.',
								icon: 'warning',
								button: "Ok"
							});
							
							$('#load-form').fadeOut();
						}
					});
					return false;
					
				//});
			}
			
			return !!validatorResult.valid;
		});
      

      
    </script>
    <!-- /validator -->


  </body>
</html>