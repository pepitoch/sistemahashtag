<?php
	session_start();
	
	if(!isset($_SESSION['iduser'])){
		header("Location: login.php");
	}

	//Data
	include_once "../data/dataBase.php";
	
	//Clases
	include_once "../objects/clases/cHome.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oHome = new Home($db);
	
	
	$vGetCita 		= $oHome->get_count_citas();
	$vGetBanner		= $oHome->get_count_banner();
	$vGetAnuncio	= $oHome->get_count_anuncio();
	
	$query 	= "select nombre, codigo_vendedor, count(codigo_vendedor) as total
					from banner 
					where fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."'
					AND (estado_banner = 1 || estado_banner = 2 || estado_banner = 3) 
					group by nombre 
					order by total desc LIMIT 10;";
					
	//echo $query;
	
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
			
			<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_anuncio.php">
						
						<div class="tile-stats bolsa-trabajo">
							<div class="icon"><i class="dark fa fa-briefcase"></i></div>
							<div class="count white"><?=$vGetAnuncio;?><small> anuncio</small> esta semana</div>
							<h3><small class="dark">Gestionar Anuncio</small> <big class="white">Bolsa de Trabajo</big></h3>
							<p class="dark">
								Aqui podras gestionar el anuncio de Bolsa de Trabajo.
							</p>
						</div>
					</a>
				</div>
				
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_anuncio_online.php">
						
						<div class="tile-stats bolsa-trabajo-1">
							<div class="icon"><i class="white fa fa-globe"></i></div>
							<div class="count dark"><small class="white"> Bolsa de Trabajo</small> ONLINE</div>
							<h3><small class="white">Gestionar Anuncio</small> <big class="white">Bolsa de Trabajo</big></h3>
							<p class="white">
								Aqui podras gestionar el anuncio de Bolsa de Trabajo ONLINE.
							</p>
						</div>
					</a>
				</div>
				
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_banners.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-picture-o"></i></div>
							<div class="count"><?=$vGetBanner;?><small> banner</small> </br>este mes</div>
							<h3>Publicar Banner</h3>
							<p>
								Aqui podras gestionar la publicacion que se han generado.
							</p>
						</div>
					</a>
				</div>
								
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_emailing.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-envelope"></i></div>
							<div class="count"><?=$vGetCita;?><small> env&iacute;os</small> </br>emailing</div>
							<h3>Solicitar Emailing</h3>
							<p>
								Aqui podras gestionar una campa&ntilde;a para el env&iacute;o de correos electronicos
								para promociones de clientes.
							</p>
						</div>
					</a>
				</div>
				<div class="clearfix"></div>
				
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_smartcontent.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-newspaper-o"></i></div>
							<div class="count"><?=$vGetCita;?><small> solicitud(es)</small> </br>Smart Content</div>
							<h3>Crear Cita</h3>
							<p>
								Aqui podras gestionar una solicitud de Smartcontent.
							</p>
						</div>
					</a>
				</div>
				
				<div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="g_citas.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-clock-o"></i></div>
							<div class="count"><?=$vGetCita;?><small> citas</small></div>
							<h3>Gestionar Citas</h3>
							<p>
								Aqui podras gestionar las citas que se han generado para poder visitar a un cliente.
							</p>
						</div>
					</a>
				</div>
			
			</div>
			
            
          </div>
          <!-- /top tiles -->

          
          <br />

          


          <div class="row">
            


            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>

  </body>
</html>