<?php
	session_start(); 
	//session_destroy();

	//Data
	include_once "data/dataBase.php";
	
	//Clases
	include_once "objects/clases/cCita.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	
	// set your default timezone
	date_default_timezone_set('America/Tegucigalpa');
	setlocale(LC_ALL,"es_SV");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

	
	//echo $_SESSION['imgFile'].'d';
	//echo phpinfo();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Sistema HT #</title>
	
	
	<?php include_once "c_css.php";?>

    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Publicar BANNER</h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			
			
            <div class="clearfix"></div>
			
			<form class="form-horizontal form-label-left" method="POST" novalidate enctype="multipart/form-data">
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Ejecutivo de Ventas</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="code-ventas">C&oacute;digo Ejecutivo de Ventas <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="code-ventas" name="code-ventas" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Completo <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="first-name" name="first-name" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							  <div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Correo Electr&oacute;nico <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback-left">
								  <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
								  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
						  </div>
					  </div>
					</div>
				</div>
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Cliente</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							

							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="cliente">Nombre de Cliente o Empresa:<span class="required">*</span>
								</label>
								<input type="text" id="cliente" name="cliente" required="required" class="form-control">
								
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12 ht-check">
								<label class="control-label" for="publicacion">Publicar Banner en: <span class="required">*</span>
								</label>
								<!--<p>
									<input type="radio" class="flat" name="publicacion" id="publicacionP" value="La Prensa Grafica" checked="" required /> La Prensa Grafica &nbsp;&nbsp;&nbsp;
									<input type="radio" class="flat" name="publicacion" id="publicacionE" value="El Economista" /> El Economista  &nbsp;&nbsp;&nbsp;
									<input type="radio" class="flat" name="publicacion" id="publicacionG" value="El Grafico" /> El Grafico  &nbsp;&nbsp;&nbsp;
								  </p>-->
								<p>
									<input type="checkbox" required="required" class="flat" name="publicacion[]" id="publicacionx" value="1"> La Prensa Grafica &nbsp;&nbsp;&nbsp;
									<input type="checkbox"  class="flat" name="publicacion[]" id="publicaciony" value="2"> El Economista  &nbsp;&nbsp;&nbsp;
									<input type="checkbox"  class="flat" name="publicacion[]" id="publicacionz" value="3"> El Grafico  &nbsp;&nbsp;&nbsp;
								</p>
								
							  </div>
							  <br />
							  <div class="item form-group col-md-6 col-sm-6 col-xs-12 ht-date">
								<label class="control-label" for="single_cal3">Elegir Rango de Fecha de Publicacion <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 has-feedback-left">
									<input readonly="readonly" required="required" type="text" class="form-control has-feedback-left" id="single_cal3" name="single_cal3" placeholder="fecha" aria-describedby="dateBanner">
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
									<span id="inputSuccess2Status3" class="sr-only">(success)</span>
									
								  </div>
							  </div>
							  <!--
								<fieldset>
								  <div class="control-group">
									<div class="controls">
									  <div class="input-prepend input-group">
										<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
										<input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="03/18/2013 - 03/23/2013" />
									  </div>
									</div>
								  </div>
								</fieldset>-->
							  
							  <div class="clearfix"></div>
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
							  <br />
								<label class="" for="url">URL / Link de Banner:</label>
								<div class="input-group">
								  <div class="input-group-addon">http://www.</div>
								  <input type="url" class="form-control" id="url" name="url" placeholder="example.com" required>
								</div>
							  </div>
							  
							  <br />
							  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
								<label class="control-label" for="tipobanner">Tipo de Banner:<span class="required">*</span>
								</label>
								<select class="form-control" required="required" id="tipobanner" name="tipobanner">
									<option value="">Seleccionar tipo de banner</option>
									<option value="Tradicional">Tradicional</option>
									<option value="Rich Media">Rich Media (banner desplegable)</option>
																		
								</select>
							  </div>
							  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
								<label class="control-label" for="impresiones">Numero de Impresiones de Banner:<span class="required">*</span>
								</label>
								<select class="form-control" required="required" id="impresiones" name="impresiones">
									<option value="">Seleccionar Cantidad de Impresiones</option>
									<option value="50000">50 MIL</option>
									<option value="100000">100 MIL</option>
									<option value="150000">150 MIL</option>
									<option value="200000">200 MIL</option>
									<option value="250000">250 MIL</option>
									<option value="300000">300 MIL</option>
									<option value="350000">350 MIL</option>
									<option value="400000">400 MIL</option>
									<option value="450000">450 MIL</option>
									<option value="500000">500 MIL</option>
									
								</select>
							  </div>
							  <div class="item form-group col-md-4 col-sm-4 col-xs-12">
								<label class="control-label" for="inversion">Inversion en Dolares:<span class="required">*</span>
								</label>
								<div class="input-group">
								  <div class="input-group-addon">USD$</div>
								  <input type="number" class="form-control" id="inversion" name="inversion" placeholder="00.00" min="0" required>
								  <div class="input-group-addon">+ IVA</div>
								</div>
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="message">Mensajes / Comentarios
								</label>
								<textarea id="message" class="form-control" name="message" rows="10" required></textarea>
								
							  </div>
							  
							  <div class="clearfix"></div>
							  <div class="ln_solid"></div>
							  
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="file">Archivos Adjuntos
								</label>
								<div class="dropzone dropzone-previews" id="my-awesome-dropzone">
									<div class="dz-message" data-dz-message>
										<span>Hacer click o arrastrar los archivos a subir</span><br/><br/>
										<span class="fa fa-upload fa-5x"></span>
										
									</div>
								</div>
								<!--<input type="hidden" value="" id="Files" name="Files" />-->
							  </div>
							  
							  
							  <div class="clearfix"></div>
							  <br/>
							  <div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" value="nBanner" name="opt" />
								  <button id="send" type="submit" class="btn btn-success btn-block btn-lg">Enviar Publicacion</button>
								</div>

							
							  </div>
						  </div>
					  </div>
					</div>
				</div>
			</form>
			
			
           

            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 
	
	<!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    

    
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
   
    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert.min.js"></script>
	<!-- Dropzone.js -->
    <script src="../vendors/dropzone/dist/min/dropzone.min.js"></script>
	
	<!-- Dropdown.js -->
	<script src="js/select2.min.js"></script>
	
	<!-- jquery.cookie.js -->
	<script src="../vendors/js-cookie-master/js-cookie-master/src/js.cookie.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

	
	<!-- Jquery cookie -->
	<script>
		if(Cookies.get('codigo')){
			$('#code-ventas').val(Cookies.get('codigo'));
			$('#first-name').val(Cookies.get('nombre'));
			$('#email').val(Cookies.get('correo'));
			$('.myname').text(Cookies.get('nombre'));
		}
	</script>
	
	<!-- Dropzone -->
	<script>
		
		
		Dropzone.autoDiscover = false;
		$(function() {
			// Now that the DOM is fully loaded, create the dropzone & configuration options
			var myDropzone = new Dropzone('div#my-awesome-dropzone', {
				// url: "objects/actionBanner.php?opt=imgBanner",
				url: "objects/actionBanner.php?opt=imgBanner",
				//maxFilesize: 9.0,
				parallelUploads: 1,
				uploadMultiple: true,
				autoProcessQueue: true
				//acceptedFiles: 'image/*'
			});
			
			 // and setup the on success event listeners
			// myDropzone.on("success", function (file) {
				// var value = $("#Files").val();
				// $("#Files").val(value + ';' + 'responsetext.Url');
				// console.log(file);
				// // var parsed = JSON.parse(data);
				// // swal({
					// // title: parsed.title,
					// // text: parsed.text,
					// // type: parsed.type,
					// // confirmButtonText: "Ok"
				// // });
			// });

		});
	
		// Dropzone.autoDiscover = false;
		// jQuery(document).ready(function() {

		  // $("div#my-awesome-dropzone").dropzone({
			// url: "objects/actionBanner.php?opt=imgBanner"
		  // });

		// });
	</script>
	
    
    <!-- picker -->
	<?php
		$datetime = new DateTime('tomorrow');
		
		$fecha = date('m/d/Y');
		$nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'm/d/Y' , $nuevafecha );
		
	?>
	<script>
		
      $(document).ready(function() {
		// $('#single_cal3').daterangepicker(null, function(start, end, label) {
			// console.log(start.toISOString(), end.toISOString(), label);
        // });
		
		// $('#reservation').daterangepicker(null, function(start, end, label) {
			// console.log(start.toISOString(), end.toISOString(), label);
        // });
		
		
		/******************************/
			
		var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
		$("#impresiones").select2({
		  //data: country,
		  tags: true
		});
		/******************************/
		
		
		  
        $('#single_cal3').daterangepicker({
			 autoUpdateInput: true,
			 "locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Aplicar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "Hasta",
				"customRangeLabel": "Modificar",
				"weekLabel": "S",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				]
			},
          singleDatePicker: false,
          calender_style: "picker_3",
		  startDate: "<?php echo date('m-d-Y');?>",
		  minDate: "<?php echo $datetime->format('m-d-Y');?>",
		  maxDate: "<?php echo $nuevafecha;?>"
        }, function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
        });
		
		$('#single_cal3').on('apply.daterangepicker', function(ev, picker) {
			//$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			$('.ht-date').removeClass('bad');
			$('.ht-date .alert').hide();
		});

		$('#single_cal3').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
		
		
		// $('#dateBanner').daterangepicker({
			
		// }, function(start, end, label) {
		  // console.log("New date range selected: " + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD') + " (predefined range: " + label + ")");
		// });
        
      });
    </script>
	
	
    <!-- validator -->
    <script>
      // initialize the validator function
     // validator.message.inversion = 'not a real date';

		var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})
		

		$('form').submit(function(e){

			
			$cbx_group = $("input:checkbox[name='publicacion[]']");
			//$cbx_group = $("input:checkbox[id^='option-']"); // name is not always helpful ;)

			$cbx_group.prop('required', true);
			if($cbx_group.is(":checked")){
			  $cbx_group.prop('required', false);
			  $('.ht-check').removeClass('bad');
			  $('.ht-check .alert').hide();
			}
			
			var submit = true, validatorResult = validator.checkAll(this);
			
			if (!validatorResult.valid) {
				console.log('holis');
				submit = false;
				
			}
			
			if (submit){
				$('#load-form').fadeIn();
				console.log('submit'); 
				
				//$(document).on('submit', '#create-business-form', function() {
					$.ajax({
					  url: "objects/actionBanner.php",
					  type: "POST",
					  data:  new FormData(this),
					  contentType: false,
					  cache: false,
					  processData:false,
					  beforeSend : function(){
						  //Dropzone.forElement("div#my-awesome-dropzone").processQueue();
					  },
					  success: function(data) {
							
							Cookies.set('codigo', $('#code-ventas').val(), { expires: 365 });
							Cookies.set('nombre', $('#first-name').val(), { expires: 365 });
							Cookies.set('correo', $('#email').val(), { expires: 365 });
							
						var parsed = JSON.parse(data);
						  swal({
							title: 'Excelente!',
							text: 'Banner creado',
							icon: 'success',
							button: "Aww yiss!"
						  });
							$('#load-form').fadeOut();
							$('form')[0].reset();
							Dropzone.forElement("div#my-awesome-dropzone").removeAllFiles(true);
							$("input[name^='publi']").prop('checked', false);
							$(".icheckbox_flat-green").removeClass('checked');
							
							if(Cookies.get('codigo')){
								$('#code-ventas').val(Cookies.get('codigo'));
								$('#first-name').val(Cookies.get('nombre'))
								$('#email').val(Cookies.get('correo'))
							}
						},
						error: function(e) { 
							swal({   
								title: 'Warning',
								text: 'Revisa la informacion proporcionada.',
								icon: 'warning',
								button: "Ok"
							});
							$('#load-form').fadeOut();
							$('form')[0].reset();
							Dropzone.forElement("div#my-awesome-dropzone").removeAllFiles(true);
							$("input[name^='publi']").prop('checked', false);
							$(".icheckbox_flat-green").removeClass('checked');
						}
					});
					return false;
					
				//});
			}
			
			return !!validatorResult.valid;
		});
      

      // $('form').submit(function(e) {
        // e.preventDefault();
        // var submit = true;

        // // evaluate the form using generic validaing
        // if (!validator.checkAll($(this))) {
          // submit = false;
        // }

        // if (submit){
			// //$(document).on('submit', '#create-business-form', function() {
				// $.ajax({
				  // url: "objects/actionBanner.php",
				  // type: "POST",
				  // data:  new FormData(this),
				  // contentType: false,
				  // cache: false,
				  // processData:false,
				  // beforeSend : function(){
				  // },
				  // success: function(data) {
					// var parsed = JSON.parse(data);
					  // swal({
						// title: parsed.title,
						// text: parsed.text,
						// type: parsed.type,
						// confirmButtonText: "Ok"
					  // });
						// $('form')[0].reset();
						// Dropzone.forElement("div#my-awesome-dropzone").removeAllFiles(true);
					// },
					// error: function(e) {
						// swal({   
							// title: parsed.title,   
							// text: parsed.text,   
							// type: parsed.type,   
							// confirmButtonText: "Ok" 
						// });
						// $('form')[0].reset();
					// }
				// });
				// return false;
				
			// //});
		// }
        

        // return false;
      // });
    </script>
    <!-- /validator -->

	
	
  </body>
</html>