<?php
	

	//Data
	include_once "data/dataBase.php";
	
	//Clases
	include_once "objects/clases/cHome.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oHome = new Home($db);
	
	
	$vGetCita 		= $oHome->get_count_citas();
	$vGetBanner		= $oHome->get_count_banner();
	$vGetAnuncio	= $oHome->get_count_anuncio();
	$vGetBannerV	= $oHome->get_banner_vendedor();
	
	
	$dato1 = array();
	$dato2 = array();
		
	foreach ($vGetBannerV AS $id => $arrDato1) {
		
		$dato1[] = substr($arrDato1['nombre'],0,15).'...';
		//$dato2[] = array($arrDato1['total'],$arrDato1['nombre']);
		$dato2[] = array(
						"value" => $arrDato1['total'],
						"name" => substr($arrDato1['nombre'],0,15).'...'
					);
	}
	//var_dump($dato2);
	$query 	= "SELECT COUNT(*) AS nbanner FROM banner WHERE fecha_creacion >= '".date('Y-m-d', strtotime('first day of this month'))."';";
	//echo $query;
	
	setlocale(LC_ALL,"es_ES");
	date_default_timezone_set('America/el_salvador');
	
	//echo (date("Y-m-d H:i:s"));
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

	
	
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Agencia Hashtag #</title>

    <?php include_once "c_css.php";?>
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
	  
        <?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
			<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="alert alert-info alert-dismissible fade in" role="alert">
					
					<strong>Bienvenido!</strong> Si necesitas ayuda con el uso del sistema puedes llamar a la extension 2342, 2343, <b>2344</b>, 2345. o escribir al correo todos@hashtag.sv <small>Holy Guacamole!</small> <a class="pull-right" href="<?=URL_HT.'admin';?>">ADMINISTRAR</a>
				</div>
			</div>
			
			<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="animated flipInY col-lg-7 col-md-12 col-sm-12 col-xs-12">
					<a href="h_anuncio.php">
						<div class="tile-stats bolsa-trabajo">
							<div class="icon"><i class="dark fa fa-briefcase"></i></div>
							<div class="count white"><?=$vGetAnuncio;?><small> anuncio</small> </br>esta semana</div>
							<h3><small class="dark">Publicar Anuncio</small> <big class="white">Bolsa de Trabajo</big></h3>
							<p class="dark"> 
								Con este formulario se hace un pedido para la creacion de un espacio para 
								publicar en la plataforma <b>Bolsa de trabajo</b>
							</p>
						</div>
					</a>
				</div>
				
				<div class="animated flipInY col-lg-5 col-md-6 col-sm-12 col-xs-12">
					<a href="h_banner.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-picture-o"></i></div>
							<div class="count"><?=$vGetBanner;?><small> banners</small> </br>este mes </div>
							<h3>Publicar Banner</h3>
							<p>
								Con este formulario se hace un pedido de la creacion de un Banner para 
								comenzar con el proceso de diseno y publicacion.
							</p>
						</div>
					</a>
				</div>
				<div class="clearfix hidden-md"></div>
				<div class="animated flipInY col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<a href="h_cita.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-clock-o"></i></div>
							<div class="count"><?=$vGetCita;?><small> citas</small> </br>esta semana</div>
							<h3>Crear Cita</h3>
							<p>
								Aqui podras crear una cita para poder visitar a un cliente
								y acompanar a un ejecutivo de ventas con el proceso de venta.
							</p>
						</div>
					</a>
				</div>
				<div class="clearfix visible-md"></div>
				<div class="animated flipInY col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<a href="h_emailing.php">
						<div class="tile-stats">
							<div class="icon"><i class="fa fa-envelope"></i></div>
							<div class="count"><?=$vGetCita;?><small> env&iacute;os</small> </br>emailing</div>
							<h3>Solicitar Emailing</h3>
							<p>
								Aqui podras crear una campa&ntilde;a para el env&iacute;o de correos electronicos
								para promociones de clientes.
							</p>
						</div>
					</a>
				</div>
				
				<div class="animated flipInY col-lg-4 col-md-6 col-sm-12 col-xs-12">
					<a href="h_cita.php">
						<div class="tile-stats">
							
							<div class="count"><?=$vGetCita;?><small> solicitud(es)</small> </br> Smart Content</div>
							<h3>Crear Smart Content</h3>
							<p>
								Aqu&iacute; podr&aacute;s crear la solicitud de entrevista y difusi&oacute;n para
								programar un SmartContent.
							</p>
						</div>
					</a>
				</div>
				
				
			
			</div>
            <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="x_panel">
                  <div class="x_title">
                    <h2>Banners publicados en el mes de <?=$meses[date('n', strtotime('this month'))-1];?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  <p>TOP 10 de Ejecutivos de Ventas que han publicado Banners</p>
                  <div class="x_content">

                    <div id="echart_donut" style="height:350px;"></div>

                  </div>
                </div>
			</div>
            <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="x_panel tile">
					<div class="x_title">
					  <h2>Contactos Hashtag</h2>
					  
					  <div class="clearfix"></div>
					</div>
					<div class="x_content">
			
						<ul class="messages">
							<!--
						  <li>
							<img src="images/imgEmi.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-info">2342</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Emi Orellana</h4>
							  <blockquote class="message">
								<label>Correo: </label> aorellana@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2342
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Estratega Digital
							  </p>
							</div>
						  </li>
						  -->
						  <!--
						  <li>
							<img src="images/imgSara.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-error">2343</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Sara Arevalo</h4>
							  <blockquote class="message">
								<label>Correo: </label> rarevalo@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2343
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Community Manager
							  </p>
							</div>
						  </li>
						  -->
						
						  <li>
							<img src="images/imgEduardo.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-error">2345</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Eduardo Contreras</h4>
							  <blockquote class="message">
								<label>Correo: </label> eturcios@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2345
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Estratega Digital
							  </p>
							</div>
						  </li>
						  <li>
							<img src="images/imgLinet.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-error">2343</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Lineth Vasquez</h4>
							  <blockquote class="message">
								<label>Correo: </label> lvasquez@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2343
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Dise&ntilde;adora Gr&aacute;fico
							  </p>
							</div>
						  </li>
						  <li>
							<img src="images/imgEli.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-error">2342</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Eli Arce</h4>
							  <blockquote class="message">
								<label>Correo: </label> earce@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2342
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Community Manager
							  </p>
							</div>
						  </li>
						  <li>
							<img src="images/imgJosue.jpg" class="avatar" alt="Avatar">
							<div class="message_date">
							  <h3 class="date text-error">2344</h3>
							  <p class="month">Ext</p>
							</div>
							<div class="message_wrapper">
							  <h4 class="heading">Josu&eacute; L&oacute;pez</h4>
							  <blockquote class="message">
								<label>Correo: </label> afigueroa@hashtag.sv<br/>
								<label>Telefono: </label> 2241-2344
							  </blockquote>
							  <br>
							  <p class="url">
								<span class="fs1" aria-hidden="true" data-icon=""></span>
								Desarrollador Web
							  </p>
							</div>
						  </li>

						</ul>
					</div>
				</div>
			</div>
          </div>
          <!-- /top tiles -->

          
          <br />

          


          <div class="row">
            


            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- ECharts -->
    <script src="../vendors/echarts/dist/echarts.min.js"></script>
    <script src="../vendors/echarts/map/js/world.js"></script>

	<!-- jquery.cookie.js -->
	<script src="../vendors/js-cookie-master/js-cookie-master/src/js.cookie.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
	<!-- Jquery cookie -->
	<script>
		if(Cookies.get('nombre')){
			$('.myname').text(Cookies.get('nombre'));
		}
	</script>
	
	<script>
	
		/* ECHRTS */
	
		
		function init_echarts() {
		
				if( typeof (echarts) === 'undefined'){ return; }
				console.log('init_echarts');
			
		
				  var theme = {
				  color: [
					  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
					  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
				  ],

				  title: {
					  itemGap: 8,
					  textStyle: {
						  fontWeight: 'normal',
						  color: '#408829'
					  }
				  },

				  dataRange: {
					  color: ['#1f610a', '#97b58d']
				  },

				  toolbox: {
					  color: ['#408829', '#408829', '#408829', '#408829']
				  },

				  tooltip: {
					  backgroundColor: 'rgba(0,0,0,0.5)',
					  axisPointer: {
						  type: 'line',
						  lineStyle: {
							  color: '#408829',
							  type: 'dashed'
						  },
						  crossStyle: {
							  color: '#408829'
						  },
						  shadowStyle: {
							  color: 'rgba(200,200,200,0.3)'
						  }
					  }
				  },

				  dataZoom: {
					  dataBackgroundColor: '#eee',
					  fillerColor: 'rgba(64,136,41,0.2)',
					  handleColor: '#408829'
				  },
				  grid: {
					  borderWidth: 0
				  },

				  categoryAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },

				  valueAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitArea: {
						  show: true,
						  areaStyle: {
							  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },
				  timeline: {
					  lineStyle: {
						  color: '#408829'
					  },
					  controlStyle: {
						  normal: {color: '#408829'},
						  emphasis: {color: '#408829'}
					  }
				  },

				  k: {
					  itemStyle: {
						  normal: {
							  color: '#68a54a',
							  color0: '#a9cba2',
							  lineStyle: {
								  width: 1,
								  color: '#408829',
								  color0: '#86b379'
							  }
						  }
					  }
				  },
				  map: {
					  itemStyle: {
						  normal: {
							  areaStyle: {
								  color: '#ddd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  },
						  emphasis: {
							  areaStyle: {
								  color: '#99d2dd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  }
					  }
				  },
				  force: {
					  itemStyle: {
						  normal: {
							  linkStyle: {
								  strokeColor: '#408829'
							  }
						  }
					  }
				  },
				  chord: {
					  padding: 4,
					  itemStyle: {
						  normal: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  },
						  emphasis: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  }
					  }
				  },
				  gauge: {
					  startAngle: 225,
					  endAngle: -45,
					  axisLine: {
						  show: true,
						  lineStyle: {
							  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
							  width: 8
						  }
					  },
					  axisTick: {
						  splitNumber: 10,
						  length: 12,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  axisLabel: {
						  textStyle: {
							  color: 'auto'
						  }
					  },
					  splitLine: {
						  length: 18,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  pointer: {
						  length: '90%',
						  color: 'auto'
					  },
					  title: {
						  textStyle: {
							  color: '#333'
						  }
					  },
					  detail: {
						  textStyle: {
							  color: 'auto'
						  }
					  }
				  },
				  textStyle: {
					  fontFamily: 'Arial, Verdana, sans-serif'
				  }
			  };

			  
			  
			   //echart Donut
			  
			if ($('#echart_donut').length ){  
			
			
			  
			  var echartDonut = echarts.init(document.getElementById('echart_donut'), theme);
			  
			  echartDonut.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} banner ({d}%)"
				},
				calculable: true,
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: <?php echo json_encode($dato1); ?>
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel'],
					  option: {
						funnel: {
						  x: '25%',
						  width: '50%',
						  funnelAlign: 'left',
						  max: 1548
						}
					  }
					},
					restore: {
					  show: true,
					  title: "Actualizar"
					},
					saveAsImage: {
					  show: true,
					  title: "Guardar Imagen",
					  name: "TOP_10_de_Ejecutivos_de_Ventas_que_han_publicado_Banners",
					  pixelRatio: 2
					}
				  }
				},
				series: [{
				  name: 'Publicacion de Banners',
				  type: 'pie',
				  radius: ['15%', '40%'],
				  
				  itemStyle: {
					normal: {
					  label: {
						show: true,
						formatter: '{d}% {b}  '
					  },
					  labelLine: {
						show: true
					  }
					},
					emphasis: {
					  label: {
						show: true,
						position: 'center',
						textStyle: {
						  fontSize: '12',
						  fontWeight: 'normal'
						}
					  }
					}
				  },
				  data: <?php echo json_encode($dato2); ?>
				}]
			  });

			} 
			  
			
	   
		} 


		$(document).ready(function() {
						
			init_echarts();
						
		});	
	</script>

  </body>
</html>