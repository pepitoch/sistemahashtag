<?php
	session_start(); 
	//session_destroy();

	//Data
	include_once "data/dataBase.php";
	
	//Clases
	include_once "objects/clases/cCita.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	
	// set your default timezone
	date_default_timezone_set('America/Tegucigalpa');
	setlocale(LC_ALL,"es_SV");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

	
	//echo $_SESSION['imgFile'].'d';
	//echo phpinfo();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Sistema HT #</title>
	
	
	<?php include_once "c_css.php";?>

    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Solicitud de Env&iacute;os de Emailing</h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			
			
            <div class="clearfix"></div>
			
			<form class="form-horizontal form-label-left" method="POST" novalidate enctype="multipart/form-data">
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Ejecutivo de Ventas</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="code-ventas">C&oacute;digo Ejecutivo de Ventas <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="code-ventas" name="code-ventas" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							
							  <div class="item form-group">

								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Completo <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="first-name" name="first-name" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
								  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							  <div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Correo Electr&oacute;nico <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback-left">
								  <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
								  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
						  </div>
					  </div>
					</div>
				</div>
				<div class="row">
				
					
					<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Cliente</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							

							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="cliente">Nombre de Cliente o Empresa:<span class="required">*</span>
								</label>
								<input onkeyup="getVal('cliente')" type="text" id="cliente" name="cliente" required="required" class="form-control">
								
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="asunto">T&iacute;tulo de ASUNTO de correo:<span class="required">*</span>
								</label>
								<input onkeyup="getVal('asunto')" type="text" id="asunto" name="asunto" required="required" class="form-control" placeholder="Ej.: 30% de descuento en toda la tienda, solo en EPA!!!!!">
								
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="remitente">Remitente / correo del cliente - empresa:<span class="required">*</span>
								</label>
								<input onkeyup="getVal('remitente')" type="text" id="remitente" name="remitente" required="required" class="form-control" placeholder="Ej.: empresa@ejemplo.com">
								
							  </div>
							  
							  <br />
							  
							  
							  <div class="clearfix"></div>
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
							  <br />
								<label class="" for="url">URL / Link de redireccion de la campa&ntilde;a:*</label>
								<div class="input-group">
								  <div class="input-group-addon">http://www.</div>
								  <input type="url" class="form-control" id="url" name="url" placeholder="example.com" required>
								</div>
							  </div>
							  
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="item form-group">
										<label>Imagen / Arte de la campana de correos: </label>
										<input type="file" class="form-control" name="filelife" id="imagenanuncio" >
										<p class="help-block">Si no existe arte este campo no es obligatorio.</p>
									</div>
								</div>
							  
							  <br />
							  <div class="item form-group col-md-6 col-sm-6 col-xs-12">
								<label class="control-label" for="envio">Numero de Env&iacute;os:<span class="required">*</span>
								</label>
								<select class="form-control" required="required" id="envio" name="envio">
									<option value="">Seleccionar n&uacute;mero de env&iacute;os</option>
									<?php
										for($i=1;$i<=20;$i++){
											echo '<option value="'.$i.'">'.$i.' env&iacute;os</option>';
										}
									?>
																		
								</select>
							  </div>
							  
							  <div class="item form-group col-md-6 col-sm-6 col-xs-12">
								<label class="control-label" for="inversion">Inversion en Dolares:<span class="required">*</span>
								</label>
								<div class="input-group">
								  <div class="input-group-addon">USD$</div>
								  <input type="number" class="form-control" id="inversion" name="inversion" placeholder="00.00" min="0" required>
								  
								</div>
							  </div>
							  
							  
							  <div class="clearfix"></div>
							  <div class="ln_solid"></div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12 ht-date">
								<label class="control-label" for="single_cal3">Fecha tentativa para iniciar env&iacute;os <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 has-feedback-left">
									<input readonly="readonly" required="required" type="text" class="form-control has-feedback-left" id="single_cal3" name="single_cal3" placeholder="fecha" aria-describedby="dateBanner">
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
									<span id="inputSuccess2Status3" class="sr-only">(success)</span>
									
								  </div>
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="message">Observaciones / Comentarios
								</label>
								<textarea id="message" class="form-control" name="message" rows="5"></textarea>
								
							  </div>
							  
							  
							  
							  
							  
							  
							  <div class="clearfix"></div>
							  <br/>
							  <div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" value="nEmailing" name="opt" />
								  <button id="send" type="submit" class="btn btn-success btn-block btn-lg">Programar Emailing</button>
								</div>

							
							  </div>
						  </div>
					  </div>
					</div>
					
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Campa&ntilde;a de Correos</h2>
							
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
							<br />
								
								<div class="row">
								  <div class="col-sm-12 mail_list_column">
									
									<a href="#">
									  <div class="mail_list">
										<div class="left">
										  <i class="fa fa-circle"></i> <i class="fa fa-edit"></i>
										</div>
										<div class="right">
										  <h3 class="txtcliente">Dennis Mugo <small>3.00 PM</small></h3>
										  <p class="txtasunto">Ut enim ad minim veniam, quis nostrud exercitation enim ad minim veniam, quis nostrud exercitation...</p>
										</div>
									  </div>
									</a>
									
								  </div>
								  <!-- /MAIL LIST -->

								  <!-- CONTENT MAIL -->
								  <div class="col-sm-12 mail_view">
									<div class="inbox-body">
									  <div class="mail_heading row">
										
										<div class="col-md-12 text-right">
										  <p class="date txtdate"> 8:02 PM 12 FEB 2014</p>
										</div>
										<div class="col-md-12">
										  <h4 class="txtasunto"> Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum.</h4>
										</div>
									  </div>
									  <div class="sender-info">
										<div class="row">
										  <div class="col-md-12">
											<strong class="txtcliente">Jon Doe</strong>
											(<span class="txtremitente">jon.doe@gmail.com</span>) para
											<strong>mi</strong>
											<a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>
										  </div>
										</div>
									  </div>
									  <div class="view-mail">
										<p class="text-center">
											<img src="http://via.placeholder.com/630x800/f7f7f7" alt="img" class="img-responsive txtimage"/>
										</p>
							
										
									  </div>
									  
									</div>

								  </div>
								  <!-- /CONTENT MAIL -->
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</form>
			
			
           

            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 
	
	<!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    

    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert.min.js"></script>
	
	
	<!-- jquery.cookie.js -->
	<script src="../vendors/js-cookie-master/js-cookie-master/src/js.cookie.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

	
	<!-- Jquery cookie -->
	<script>
		if(Cookies.get('codigo')){
			$('#code-ventas').val(Cookies.get('codigo'));
			$('#first-name').val(Cookies.get('nombre'));
			$('#email').val(Cookies.get('correo'));
			$('.myname').text(Cookies.get('nombre'));
		}
	</script>
	
	
	<!-- get text -->
	<script>
		
		function getVal(id_class) {
			var x = document.getElementById(id_class);
			$(".txt"+id_class).text($(x).val())
			
		}
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function (e) {
					$('.txtimage').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#imagenanuncio").change(function(){
			readURL(this);
		});
	</script>
	
	
	
    
    <!-- picker -->
	<?php
		$datetime = new DateTime('tomorrow');
		
		$fecha = date('m/d/Y');
		$nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'm/d/Y' , $nuevafecha );
		
	?>
	<script>
		
      $(document).ready(function() {
				  
        $('#single_cal3').daterangepicker({
			 autoUpdateInput: true,
			 "locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Aplicar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "Hasta",
				"customRangeLabel": "Modificar",
				"weekLabel": "S",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				]
			},
          singleDatePicker: true,
          calender_style: "picker_3",
		  startDate: "<?php echo date('m-d-Y');?>",
		  minDate: "<?php echo $datetime->format('m-d-Y');?>",
		  maxDate: "<?php echo $nuevafecha;?>"
        }, function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
			$('.txtdate').text($('#single_cal3').val());
        });
		
		$('#single_cal3').on('apply.daterangepicker', function(ev, picker) {
			//$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			$('.ht-date').removeClass('bad');
			$('.ht-date .alert').hide();
		});

		$('#single_cal3').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
		
        
      });
    </script>
	
	
    <!-- validator -->
    <script>
      // initialize the validator function
     // validator.message.inversion = 'not a real date';

		var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})
		

		$('form').submit(function(e){

			var submit = true, validatorResult = validator.checkAll(this);
			
			if (!validatorResult.valid) {
				console.log('holis');
				submit = false;
				
			}
			
			if (submit){
				$('#load-form').fadeIn();
				console.log('submit'); 
				
				//$(document).on('submit', '#create-business-form', function() {
					$.ajax({
					  url: "objects/actionEmailing.php",
					  type: "POST",
					  data:  new FormData(this),
					  contentType: false,
					  cache: false,
					  processData:false,
					  beforeSend : function(){
						  //Dropzone.forElement("div#my-awesome-dropzone").processQueue();
					  },
					  success: function(data) {
							
							Cookies.set('codigo', $('#code-ventas').val(), { expires: 365 });
							Cookies.set('nombre', $('#first-name').val(), { expires: 365 });
							Cookies.set('correo', $('#email').val(), { expires: 365 });
							
						var parsed = JSON.parse(data);
							swal({
								title: parsed.title,
								text: parsed.text,
								icon: parsed.type,
								button: "Aww yiss!"
							}).then((value) => {
								window.location.replace("index.php");
							});
							$('#load-form').fadeOut();
							$('form')[0].reset();
							
							
							if(Cookies.get('codigo')){
								$('#code-ventas').val(Cookies.get('codigo'));
								$('#first-name').val(Cookies.get('nombre'))
								$('#email').val(Cookies.get('correo'))
							}
						},
						error: function(e) { 
							swal({   
								title: 'Warning',
								text: 'Revisa la informacion proporcionada.',
								icon: 'warning',
								button: "Ok"
							});
							$('#load-form').fadeOut();
							$('form')[0].reset();
							
						}
					});
					return false;
					
				//});
			}
			
			return !!validatorResult.valid;
		});
      

    </script>
    <!-- /validator -->

	
	
  </body>
</html>