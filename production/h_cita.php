<?php
	

	//Data
	include_once "data/dataBase.php";
	
	//Clases
	include_once "objects/clases/cCita.php";
	
	$database = new Database();
	$db = $database->getConnection();
	
	$oCita = new Cita($db);
	
	
	$vCita 		= $oCita->get_date();
	$vCitaT 	= $oCita->get_date_t();
	
	setlocale(LC_ALL,"es_ES");
					
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title># Sistema HT #</title>
	
	<?php include_once "c_css.php";?>
    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once "menu.php";?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Cita con Agencia Hashtag</h3>
              </div>

              <div class="title_right" style="visibility:hidden;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			
			<div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Citas Ma&ntilde;ana <small> fechas a partir de ma&ntilde;ana</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  
                    <?php
					
					if($vCita){
						foreach ($vCita AS $id => $arrCita) {
					?>
						
						<article class="media event">
						  <a class="pull-left date">
							<p class="month"><?=$meses[date('n', $arrCita['fecha_cita'])-1];?></p>
							<p class="day"><?=date('d', $arrCita['fecha_cita']);?></p>
						  </a>
						  <div class="media-body">
							<a class="title" href="#"><?=$arrCita['nombre'];?></a>
							<p><?=$arrCita['hora_cita'];?></p>
							<p><?php
								
								echo $dias[date('w', $arrCita['fecha_cita'])]." ".date('d', $arrCita['fecha_cita'])." de ".$meses[date('n', $arrCita['fecha_cita'])-1]. " del ".date('Y', $arrCita['fecha_cita']) ;
							?></p>
						  </div>
						</article>
							
							
					<?php
						}
					} else {
						echo "<p>No hay citas en la manana para este mes.</p>";
					}
					?>
                  </div>
                </div>

                


              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Citas Tarde <small> fechas a partir de ma&ntilde;ana</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php
					
					if($vCitaT){
						foreach ($vCitaT AS $id => $arrCita) {
					?>
						
						<article class="media event">
						  <a class="pull-left date">
							<p class="month"><?=$meses[date('n', $arrCita['fecha_cita'])-1];?></p>
							<p class="day"><?=date('d', $arrCita['fecha_cita']);?></p>
						  </a>
						  <div class="media-body">
							<a class="title" href="#"><?=$arrCita['nombre'];?></a>
							<p><?=$arrCita['hora_cita'];?></p>
							<p><?php
								
								echo $dias[date('w', $arrCita['fecha_cita'])]." ".date('d', $arrCita['fecha_cita'])." de ".$meses[date('n', $arrCita['fecha_cita'])-1]. " del ".date('Y', $arrCita['fecha_cita']) ;
							?></p>
						  </div>
						</article>
							
							
					<?php
						}
					} else { 
						echo "<p>No hay citas en la tarde para este mes.</p>";
					}
					?>
                  </div>
                </div>
              </div>


              
            </div>
			
            <div class="clearfix"></div>
			
			<form class="form-horizontal form-label-left" method="POST" novalidate>
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Ejecutivo de Ventas</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
								<div class="item form-group">

									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="code-ventas">C&oacute;digo Ejecutivo de Ventas <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
									  <input type="text" id="code-ventas" name="code-ventas" class="form-control has-feedback-left col-md-7 col-xs-12" required="required">
									  <span class="fa fa-code form-control-feedback left" aria-hidden="true"></span>
									</div>
								</div>

							  <div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Completo <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
								  <input type="text" id="first-name" name="first-name" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
								  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
							  <div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Correo Electr&oacute;nico <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12 has-feedback-left">
								  <input type="email" id="email" name="email" required="required" class="form-control has-feedback-left col-md-7 col-xs-12">
								  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
								</div>
							  </div>
						  </div>
					  </div>
					</div>
				</div>
				<div class="row">
				  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Informaci&oacute;n de Cliente</h2>
							
							<div class="clearfix"></div>
						  </div>
						  <div class="x_content">
							<br />
							

							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="cliente">Nombre de Cliente o Empresa <span class="required">*</span>
								</label>
								<input type="text" id="cliente" name="cliente" required="required" class="form-control">
								
							  </div>
							  <br />
							  <div class="item form-group col-md-6 col-sm-6 col-xs-12 ht-date">
								<label class="control-label" for="single_cal3">Fecha de la Cita a Programar <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 has-feedback-left">
									<input readonly="readonly" required="required" type="text" class="form-control has-feedback-left" id="single_cal3" name="single_cal3" placeholder="fecha" aria-describedby="dateBanner">
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
									<span id="inputSuccess2Status3" class="sr-only">(success)</span>
									
								  </div>
							  </div>
							  <div class="item form-group col-md-6 col-sm-6 col-xs-12">
								<label class="control-label" for="hora">Hora de la Cita a Programar <span class="required">*</span>
								</label>
								
								<select class="form-control" required="required" id="hora" name="hora">
									<option value="">Seleccionar Hora</option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="08:30 AM">08:30 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="09:30 AM">09:30 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="10:30 AM">10:30 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="11:30 AM">11:30 AM</option>
									<option value="12:00 PM">12:00 PM</option>
									<option value="12:30 PM">12:30 PM</option>
									<option value="01:00 PM">01:00 PM</option>
									<option value="01:30 PM">01:30 PM</option>
									<option value="02:00 PM">02:00 PM</option>
									<option value="02:30 PM">02:30 PM</option>
									<option value="03:00 PM">03:00 PM</option>
									<option value="03:30 PM">03:30 PM</option>
									<option value="04:00 PM">04:00 PM</option>
									<option value="04:30 PM">04:30 PM</option>
									<option value="05:00 PM">05:00 PM</option>
								</select>
								
							  </div>
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="historial">Historial del Cliente <span class="required">*</span>
								</label>
								<textarea id="historial" required="required" class="form-control" name="historial"></textarea>
								
							  </div>
							  
							  <div class="clearfix"></div>
							  <div class="ln_solid"></div>
							  
							  
							  
							  <div class="item form-group col-md-12 col-sm-12 col-xs-12">
								<label class="control-label" for="message">Comentarios Adicionales
								</label>
								<textarea id="message" class="form-control" name="message"></textarea>
								
							  </div>
							  
							  <div class="clearfix"></div>
							  <br/>
							  <div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" value="nCita" name="opt" />
								  <button id="send" type="submit" class="btn btn-success btn-block btn-lg">Enviar Cita</button>
								</div>

							
							  </div>
						  </div>
					  </div>
					</div>
				</div>
			</form>
			
			
            

            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Solicitud HT <a href="hashtag.sv">hashtag.sv</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
	<div id="load-form">
		<span class="fa fa-spinner fa-spin fa-3x fa-fw"></span>
	</div> 

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert.min.js"></script>

	<!-- jquery.cookie.js -->
	<script src="../vendors/js-cookie-master/js-cookie-master/src/js.cookie.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

	
	<!-- Jquery cookie -->
	<script>
		if(Cookies.get('codigo')){
			$('#code-ventas').val(Cookies.get('codigo'));
			$('#first-name').val(Cookies.get('nombre'));
			$('#email').val(Cookies.get('correo'));
			$('.myname').text(Cookies.get('nombre'));
		}
	</script>
    
    <!-- picker -->
	<!-- picker -->
	<?php
		$datetime = new DateTime('tomorrow');
		
		$fecha = date('m/d/Y');
		$nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'm/d/Y' , $nuevafecha );
		
	?>
	<script>
		
      $(document).ready(function() {
        // $('#single_cal3').daterangepicker({
          // singleDatePicker: true,
          // calender_style: "picker_3",
		  // minDate: "<?php echo $datetime->format('m-d-Y');?>"
        // }, function(start, end, label) {
         // // console.log(start.toISOString(), end.toISOString(), label);
        // });
		
		$('#single_cal3').daterangepicker({
			 autoUpdateInput: true,
			 "locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Aplicar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "Hasta",
				"customRangeLabel": "Modificar",
				"weekLabel": "S",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				]
			},
          singleDatePicker: true,
          calender_style: "picker_3",
		  minDate: "<?php echo $datetime->format('m-d-Y');?>",
		  maxDate: "<?php echo $nuevafecha;?>"
        }, function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
        });
		
		$('#single_cal3').on('apply.daterangepicker', function(ev, picker) {
			//$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			$('.ht-date').removeClass('bad');
			$('.ht-date .alert').hide();
		});

		$('#single_cal3').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
        
      });
    </script>
	
	
    <!-- validator -->
    <script>
      // initialize the validator function
      var validator = new FormValidator(
			{
				invalid         : 'inupt is not as expected',
				short           : 'input is too short',
				long            : 'input is too long',
				checked         : 'Al menos uno debe ser seleccionado',
				empty           : 'por favor completar campo',
				select          : 'Please select an option',
				number_min      : 'too low',
				number_max      : 'too high',
				url             : 'URL invalida',
				number          : 'no es un numero',
				email           : 'email is invalido',
				email_repeat    : 'emails do not match',
				date            : 'invalid date',
				password_repeat : 'passwords do not match',
				no_match        : 'no match',
				complete        : 'input is not complete'
			}
		);
		
		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required, input#single_cal3', function(){
				validator.checkField.call(validator, this)
			})
			.on('change', 'select.required', function(){
				validator.checkField.call(validator, this)
			})
			.on('keypress', 'input[required][pattern]', function(){
				validator.checkField.call(validator, this)
			})

      

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true, validatorResult = validator.checkAll(this);

        // evaluate the form using generic validaing
        if (!validatorResult.valid) {
			//console.log('holis');
			submit = false;
			
		}

        if (submit){
			$('#load-form').fadeIn();
			//$(document).on('submit', '#create-business-form', function() {
				$.ajax({
				  url: "objects/action.php",
				  //url: "http://127.0.0.1/SistemaHashtag/production/objects/action.php",
				  type: "POST",
				  data:  new FormData(this),
				  contentType: false,
				  cache: false,
				  processData:false,
				  beforeSend : function(){
				  },
				  success: function(data) {
					//var parsed = JSON.parse(data);
					Cookies.set('codigo', $('#code-ventas').val(), { expires: 365 });
					Cookies.set('nombre', $('#first-name').val(), { expires: 365 });
					Cookies.set('correo', $('#email').val(), { expires: 365 });
					
					  swal({
						title: 'EXCELENTE',
						text: 'Cita Creada',
						icon: 'success',
						button: "Aww yiss!"
					  }).then((value) => {
							location.reload();
						});
						$('form')[0].reset();
						
						if(Cookies.get('codigo')){
							$('#code-ventas').val(Cookies.get('codigo'));
							$('#first-name').val(Cookies.get('nombre'))
							$('#email').val(Cookies.get('correo'))
						}
					},
					error: function(e) {
						swal({   
							title: 'Warning',
							text: 'Revisa la informacion proporcionada.',
							icon: 'warning',
							button: "Ok"
						});
						$('form')[0].reset();
					}
				});
				return false;
				
			//});
		}
        

        return !!validatorResult.valid;
      });
    </script>
    <!-- /validator -->

	
	
  </body>
</html></html>