


<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
	  <a href="index.php" class="site_title"><i class="fa fa-hashtag"></i> <span>Solicitudes HT</span></a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -->
	<div class="profile">
	  <div class="profile_pic">
		<img src="images/hashtag.png" alt="..." class="img-circle profile_img">
	  </div>
	  <div class="profile_info">
		<span>Bienvenido,</span>
		<h2 class="myname">Ejecutivo</h2>
	  </div>
	</div>
	<!-- /menu profile quick info -->

	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	  <div class="menu_section">
		<h3>-</h3>
		<ul class="nav side-menu">
			<li>
				<a href="index.php">
					<i class="fa fa-home"></i> Inicio
				</a>
			</li>
			<li>
				<a href="h_cita.php">
					<i class="fa fa-clock-o"></i> Crear Cita con Agencia #
				</a>
			</li>
			<li>
				<a href="h_banner.php">
					<i class="fa fa-picture-o"></i> Publicar BANNER
				</a>
			</li>
			<li class="bolsa-trabajo">
				<a href="h_anuncio.php" class="dark">
					<i class="fa fa-briefcase"></i> Publicar Bolsa de Trabajo
				</a>
			</li>
			<li>
				<a href="h_emailing.php">
					<i class="fa fa-envelope"></i> Env&iacute;os Emailing
				</a>
			</li>
			<li>
				<a href="h_smartcontent.php">
					<i class="fa fa-newspaper-o"></i> Solicitud SmartContent
				</a>
			</li>
		  
		</ul>
	  </div>

	</div>
	<!-- /sidebar menu -->

	
	
  </div>
</div>

<!-- top navigation -->
<div class="top_nav navbar-fixed-top">
  <div class="nav_menu">
	<nav>
	  <div class="nav toggle">
		<a id="menu_toggle"><i class="fa fa-bars"></i></a>
	  </div>

	  <ul class="nav navbar-nav navbar-right">
		<li class="">
		  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<img src="images/hashtag.png" alt=""><span class="myname"></span>
			
		  </a>
		  
		</li>

		
	  </ul>
	</nav>
  </div>
</div>
<!-- /top navigation -->


